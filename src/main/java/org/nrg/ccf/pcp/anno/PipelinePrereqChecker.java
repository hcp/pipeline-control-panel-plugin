package org.nrg.ccf.pcp.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PipelinePrereqChecker {
	
	String PIPELINE_PREREQ_CHECKER = "pipelinePrereqChecker";
	
	String description() default "";

}
