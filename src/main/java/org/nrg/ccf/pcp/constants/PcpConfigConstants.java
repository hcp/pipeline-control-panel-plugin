package org.nrg.ccf.pcp.constants;

import java.util.Arrays;
import java.util.List;

public class PcpConfigConstants {
	
	public static final String CONFIG_TOOL = "pipelineControlPanel";
	public static final String CONFIG_PATH = "pipelinesJson";
	public static final String UPDATE_FREQUENCY = "pipelineUpdateFrequency";
	public static final String FREQUENCY_15_MINUTES = "15_Minutes";
	public static final String FREQUENCY_30_MINUTES = "30_Minutes";
	public static final String FREQUENCY_HOURLY = "Hourly";
	public static final String FREQUENCY_3_HOURS = "3_Hours";
	public static final String FREQUENCY_6_HOURS = "6_Hours";
	public static final String FREQUENCY_12_HOURS = "12_Hours";
	public static final String FREQUENCY_DAILY = "Daily";
	public static final String FREQUENCY_WEEKLY = "Weekly";
	public static final String FREQUENCY_MONTHLY = "Monthly";
	public static final String FREQUENCY_NO_UPDATE = "No_Update";
	public static final String FREQUENCY_DISABLE_CACHING = "Disable_Caching";
	public static final List<String> UPDATE_FREQUENCY_OPTIONS = 
			Arrays.asList(new String[]{FREQUENCY_15_MINUTES, FREQUENCY_30_MINUTES, FREQUENCY_HOURLY, 
					FREQUENCY_3_HOURS, FREQUENCY_6_HOURS, FREQUENCY_12_HOURS, FREQUENCY_DAILY, FREQUENCY_WEEKLY, FREQUENCY_MONTHLY,
					FREQUENCY_NO_UPDATE, FREQUENCY_DISABLE_CACHING});
	public static final long UP_TIME_WAIT = 450000;
	
}
