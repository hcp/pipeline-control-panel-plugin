package org.nrg.ccf.pcp.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.dto.ExecutionGroup;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

@Repository
public class PcpStatusEntityDAO extends AbstractHibernateDAO<PcpStatusEntity> {

	@SuppressWarnings("unchecked")
	public List<PcpStatusEntity> getProjectStatus(String projectId) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("project",projectId));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<PcpStatusEntity> getProjectPipelineStatus(String projectId, String pipelineId) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("project",projectId));
		criteria.add(Restrictions.eq("pipeline",pipelineId));
		return criteria.list();
	}

	public PcpStatusEntity getStatusEntity(String projectId, String pipelineId, ExecutionGroup executionGroup) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("project",projectId));
		criteria.add(Restrictions.eq("pipeline",pipelineId));
		criteria.add(Restrictions.or(
				Restrictions.eq("entityId",executionGroup.getEntityId()),
				Restrictions.eq("entityLabel",executionGroup.getEntityId())
		));
		criteria.add(Restrictions.eq("subGroup",executionGroup.getSubGroup()));
		final PcpStatusEntity statusEntity = (PcpStatusEntity)criteria.uniqueResult();
		return statusEntity;
	}

	@SuppressWarnings("unchecked")
	public List<PcpStatusEntity> getStatusEntities(String projectId, String pipelineId, String entityIdOrLabel) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("project",projectId));
		criteria.add(Restrictions.eq("pipeline",pipelineId));
		criteria.add(Restrictions.or(
				Restrictions.eq("entityId",entityIdOrLabel),
				Restrictions.eq("entityLabel",entityIdOrLabel)
		));
		return criteria.list();
	}

	public PcpStatusEntity getStatusEntity(PcpCondensedStatusI entity) {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.eq("project",entity.getProject()));
		criteria.add(Restrictions.eq("pipeline",entity.getPipeline()));
		criteria.add(Restrictions.or(
				Restrictions.eq("entityId",entity.getEntityId()),
				Restrictions.eq("entityLabel",entity.getEntityLabel())
		));
		criteria.add(Restrictions.eq("subGroup",entity.getSubGroup()));
		final PcpStatusEntity statusEntity = (PcpStatusEntity)criteria.uniqueResult();
		return statusEntity;
	}

	@SuppressWarnings("unchecked")
	public List<PcpStatusEntity> getAllSubmittedOrRunning() {
		final Criteria criteria = getCriteriaForType();
		criteria.add(Restrictions.or(
				Restrictions.eq("status",PcpConstants.PcpStatus.SUBMITTED.toString()),
				Restrictions.eq("status",PcpConstants.PcpStatus.RUNNING.toString())
		));
		return criteria.list();
	}

}
