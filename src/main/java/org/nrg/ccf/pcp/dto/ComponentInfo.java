package org.nrg.ccf.pcp.dto;

import java.util.List;
import java.util.Map;

public class ComponentInfo {

	private Map<String, List<String>> componentMap;
	private Map<String, List<String>> componentConfig;

	public ComponentInfo(Map<String, List<String>> componentMap, Map<String, List<String>> _componentConfig) {
		this.componentMap = componentMap;
		this.componentConfig = _componentConfig;
	}

	public Map<String, List<String>> getComponentMap() {
		return componentMap;
	}

	public Map<String, List<String>> getComponentConfig() {
		return componentConfig;
	}

}
