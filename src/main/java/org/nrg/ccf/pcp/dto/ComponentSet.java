package org.nrg.ccf.pcp.dto;

import org.nrg.ccf.pcp.inter.PipelineExecManagerI;
import org.nrg.ccf.pcp.inter.PipelineSelectorI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineStatusUpdaterI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;

public class ComponentSet {
	
	PipelineExecManagerI execManager;
	PipelineSelectorI selector;
	PipelineSubmitterI submitter;
	PipelineStatusUpdaterI statusUpdater;
	PipelinePrereqCheckerI prereqChecker;
	PipelineValidatorI validator;

	public ComponentSet() {
		super();
	}
	
	public ComponentSet(PipelineExecManagerI execManager, PipelineSelectorI selector, PipelineSubmitterI submitter,
			PipelineStatusUpdaterI statusUpdater, PipelinePrereqCheckerI prereqChecker, PipelineValidatorI validator) {
		super();
		this.execManager = execManager;
		this.selector = selector;
		this.submitter = submitter;
		this.statusUpdater = statusUpdater;
		this.prereqChecker = prereqChecker;
		this.validator = validator;
	}

	public PipelineExecManagerI getExecManager() {
		return execManager;
	}

	public void setExecManager(PipelineExecManagerI execManager) {
		this.execManager = execManager;
	}

	public PipelineSelectorI getSelector() {
		return selector;
	}

	public void setSelector(PipelineSelectorI selector) {
		this.selector = selector;
	}

	public PipelineSubmitterI getSubmitter() {
		return submitter;
	}

	public void setSubmitter(PipelineSubmitterI submitter) {
		this.submitter = submitter;
	}

	public PipelineStatusUpdaterI getStatusUpdater() {
		return statusUpdater;
	}

	public void setStatusUpdater(PipelineStatusUpdaterI statusUpdater) {
		this.statusUpdater = statusUpdater;
	}

	public PipelinePrereqCheckerI getPrereqChecker() {
		return prereqChecker;
	}

	public void setPrereqChecker(PipelinePrereqCheckerI prereqChecker) {
		this.prereqChecker = prereqChecker;
	}

	public PipelineValidatorI getValidator() {
		return validator;
	}

	public void setValidator(PipelineValidatorI validator) {
		this.validator = validator;
	}
	
}
