package org.nrg.ccf.pcp.dto;

public class ExecutionGroup {
	
	private String entityId;
	private String subGroup;
	
	public ExecutionGroup(String entityId, String subGroup) {
		this.entityId = entityId;
		this.subGroup = subGroup;
	}
	
	public String getEntityId() {
		return entityId;
	}
	
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	public String getSubGroup() {
		return subGroup;
	}
	
	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}

}
