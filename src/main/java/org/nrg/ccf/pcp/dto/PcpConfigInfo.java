package org.nrg.ccf.pcp.dto;

import java.util.Map;

public class PcpConfigInfo {
	
	private String pipeline;
	private String description;
	private String updateFrequency;
	private String selector;
	private String submitter;
	private String statusUpdater;
	private String prereqChecker;
	private String validator;
	private String execManager;
	private Map<String,Object> configurables;
	
	public String getPipeline() {
		return pipeline;
	}
	
	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getUpdateFrequency() {
		return updateFrequency;
	}
	
	public void setUpdateFrequency(String updateFrequency) {
		this.updateFrequency = updateFrequency;
	}
	
	public String getSelector() {
		return selector;
	}
	
	public void setSelector(String selector) {
		this.selector = selector;
	}
	
	public String getSubmitter() {
		return submitter;
	}
	
	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}
	
	public String getStatusUpdater() {
		return statusUpdater;
	}
	
	public void setStatusUpdater(String statusUpdater) {
		this.statusUpdater = statusUpdater;
	}
	
	public String getPrereqChecker() {
		return prereqChecker;
	}
	
	public void setPrereqChecker(String prereqChecker) {
		this.prereqChecker = prereqChecker;
	}
	
	public String getValidator() {
		return validator;
	}
	
	public void setValidator(String validator) {
		this.validator = validator;
	}
	
	public String getExecManager() {
		return execManager;
	}
	
	public void setExecManager(String execManager) {
		this.execManager = execManager;
	}
	
	public Map<String,Object> getConfigurables() {
		return configurables;
	}
	
	public void setConfigurables(Map<String,Object> configurables) {
		this.configurables = configurables;
	}
	
}
