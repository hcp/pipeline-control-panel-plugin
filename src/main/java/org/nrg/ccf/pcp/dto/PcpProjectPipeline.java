package org.nrg.ccf.pcp.dto;

public class PcpProjectPipeline {

	private String project;
	private String pipeline;

	public PcpProjectPipeline(String project, String pipeline) {
		this.project = project;
		this.pipeline = pipeline;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getPipeline() {
		return pipeline;
	}

	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pipeline == null) ? 0 : pipeline.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PcpProjectPipeline other = (PcpProjectPipeline) obj;
		if (pipeline == null) {
			if (other.pipeline != null)
				return false;
		} else if (!pipeline.equals(other.pipeline))
			return false;
		if (project == null) {
			if (other.project != null)
				return false;
		} else if (!project.equals(other.project))
			return false;
		return true;
	}
	
}
