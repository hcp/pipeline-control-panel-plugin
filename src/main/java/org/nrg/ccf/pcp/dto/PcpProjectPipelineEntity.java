package org.nrg.ccf.pcp.dto;

public class PcpProjectPipelineEntity {

	private String project;
	private String pipeline;
	private String entityId;

	public PcpProjectPipelineEntity(String project, String pipeline, String entityId) {
		this.project = project;
		this.pipeline = pipeline;
		this.setEntityId(entityId);
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getPipeline() {
		return pipeline;
	}

	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((pipeline == null) ? 0 : pipeline.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PcpProjectPipelineEntity other = (PcpProjectPipelineEntity) obj;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		if (pipeline == null) {
			if (other.pipeline != null)
				return false;
		} else if (!pipeline.equals(other.pipeline))
			return false;
		if (project == null) {
			if (other.project != null)
				return false;
		} else if (!project.equals(other.project))
			return false;
		return true;
	}
	
}
