package org.nrg.ccf.pcp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcpSettingsInfo {
	
	private Boolean pcpEnabled;
	private String accessByRole;
	private String accessByUser;

}
