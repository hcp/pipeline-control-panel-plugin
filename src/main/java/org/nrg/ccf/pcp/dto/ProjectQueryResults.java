package org.nrg.ccf.pcp.dto;

import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProjectQueryResults {
	
	private Date timestamp;
	private String project;
	private String pipeline;
	
}
