package org.nrg.ccf.pcp.exception;

public class PcpUpdateRunningException extends PcpException {

	private static final long serialVersionUID = 6730341569388865562L;

	public PcpUpdateRunningException() {
		super();
	}

	public PcpUpdateRunningException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PcpUpdateRunningException(String message, Throwable cause) {
		super(message, cause);
	}

	public PcpUpdateRunningException(String message) {
		super(message);
	}

	public PcpUpdateRunningException(Throwable cause) {
		super(cause);
	}

}
