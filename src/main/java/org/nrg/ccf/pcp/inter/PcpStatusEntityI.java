package org.nrg.ccf.pcp.inter;

import java.util.Date;

public interface PcpStatusEntityI {
	
	public String getProject();

	public void setProject(String project);

	public String getPipeline();

	public void setPipeline(String pipeline);
	
	public String getEntityType();

	public void setEntityType(String entityType);
	
	public String getEntityId();

	public void setEntityId(String entityId);
	
	public String getEntityLabel();

	public void setEntityLabel(String entityLabel);

	public String getSubGroup();

	public void setSubGroup(String subGroup);

	public Date getSubmitTime();

	public void setSubmitTime(Date submitTime);

	public String getSubmitNode();

	public void setSubmitNode(String submitNode);

	public String getSubmitExecManager();

	public void setSubmitExecManager(String submitExecManager);

	public String getStatus();

	public void setStatus(String status);

	public Date getStatusTime();

	public void setStatusTime(Date statusTime);

	public String getStatusInfo();

	public void setStatusInfo(String statusInfo);

	public Boolean getPrereqs();

	public void setPrereqs(Boolean prereqs);

	public Date getPrereqsTime();

	public void setPrereqsTime(Date prereqsTime);

	public String getPrereqsInfo();

	public void setPrereqsInfo(String prereqsInfo);

	public Boolean getValidated();

	public void setValidated(Boolean validated);

	public Date getValidatedTime();

	public void setValidatedTime(Date validatedTime);

	public String getValidatedInfo();

	public void setValidatedInfo(String validatedInfo);

	public Boolean getIssues();

	public void setIssues(Boolean issues);

	public Date getIssuesTime();

	public void setIssuesTime(Date issuesTime);

	public String getIssuesInfo();

	public void setIssuesInfo(String issuesInfo);

	public Boolean getImpeded();

	public void setImpeded(Boolean impeded);

	public Date getImpededTime();

	public void setImpededTime(Date impededTime);

	public String getImpededInfo();

	public void setImpededInfo(String impededInfo);

	public String getNotes();

	public void setNotes(String notes);

}
