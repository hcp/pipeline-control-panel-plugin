package org.nrg.ccf.pcp.inter;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xft.security.UserI;

public interface PipelinePrereqCheckerI {
	
	void checkPrereqs(PcpStatusEntity statusEntity, UserI user);

}
