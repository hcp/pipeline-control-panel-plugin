package org.nrg.ccf.pcp.preferences;

import java.util.List;

import javax.annotation.PostConstruct;

import org.nrg.ccf.common.utilities.abst.AbstractProjectPreferenceBean;
import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.exceptions.UnknownToolId;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@NrgPreferenceBean(toolId = "pcpProjectPreferences", toolName = "Pipeline Control Panel Plugin Project Preferences",
			description = "Manages Pipeline Control Panel plugin project preferences", strict = true)
public class PcpPreferences extends AbstractProjectPreferenceBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6101197380831598181L;
	public static final String PCP_ENABLED = "pcpEnabled";
	public static final String ACCESS_BY_ROLE = "accessByRole";
	public static final String ACCESS_BY_USER = "accessByUser";
	private static final String _pcpEnabledProjectsQuery = "SELECT DISTINCT entity_id FROM xhbm_preference WHERE name = '" + PCP_ENABLED + "' and value='true'";
	private final JdbcTemplate _jdbcTemplate;
	
	@Override
	@PostConstruct
	public void initIt() {
		super.initIt();
		// TODO:  Temorary fix data for when tool ID had wrong value (Can be removed)
		final NrgPreferenceBean preferenceBean = this.getClass().getAnnotation(NrgPreferenceBean.class);
		final String toolId = preferenceBean.toolId();
		final Long oldToolId = _jdbcTemplate.queryForObject("SELECT id FROM xhbm_tool WHERE tool_id='ccfSessionBuilding'", Long.class);
		final Long newToolId = _jdbcTemplate.queryForObject("SELECT id FROM xhbm_tool WHERE tool_id=?", Long.class, toolId);
		_jdbcTemplate.update("UPDATE xhbm_preference SET tool=? WHERE tool=? and name=?", new Object[] {newToolId, oldToolId, PCP_ENABLED});
	}
	
	@Autowired
	public PcpPreferences(NrgPreferenceService preferenceService, JdbcTemplate jdbcTemplate, PreferenceUtils preferenceUtils) {
		super(preferenceService, preferenceUtils);
		_jdbcTemplate = jdbcTemplate;
	}
	
	// Make PreferenceBeanHelper happy.  It wants the annotation to be on a getter with no arguments.
	@NrgPreference
	@SuppressFBWarnings
	public Boolean getPcpEnabled() {
		return null;
	}
	
	public Boolean getPcpEnabled(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, PCP_ENABLED);
	}
	
	public void setPcpEnabled(final String entityId, final boolean enabled) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		if (entityId==null || entityId.length()<1) {
			// Don't set value.  This is a project preference.
			log.debug("PCP Enabled preference - empty/null entity value passed");
			return;
		}
		try {
			log.debug("PCP Enabled preference - Setting value for entity=" + entityId);
			this.setBooleanValue(SCOPE, entityId, enabled, PCP_ENABLED);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			log.error("Exception setting pipeline control panel enabled preference", e);
		}
	}
	
	public List<String> getPcpEnabledProjects() {
		return _jdbcTemplate.queryForList(_pcpEnabledProjectsQuery, String.class);
	}
	
	// Make PreferenceBeanHelper happy.  It wants the annotation to be on a getter with no arguments.
	@NrgPreference
	@SuppressFBWarnings
	public String getAccessByRole() {
		return null;
	}
	
	public String getAccessByRole(final String entityId) {
		return this.getValue(SCOPE, entityId, ACCESS_BY_ROLE);
	}
	
	public void setAccessByRole(final String entityId, final String accessByRole) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		if (entityId==null || entityId.length()<1) {
			// Don't set value.  This is a project preference.
			log.debug("PCP AccessByRole preference - empty/null entity value passed");
			return;
		}
		try {
			log.debug("PCP AccessByRole preference - Setting value for entity=" + entityId);
			this.set(SCOPE, entityId, accessByRole, ACCESS_BY_ROLE);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			log.error("Exception setting pipeline control panel AccessByRole preference", e);
		}
	}
	
	// Make PreferenceBeanHelper happy.  It wants the annotation to be on a getter with no arguments.
	@NrgPreference
	@SuppressFBWarnings
	public String getAccessByUser() {
		return null;
	}
	
	public String getAccessByUser(final String entityId) {
		return this.getValue(SCOPE, entityId, ACCESS_BY_USER);
	}
	
	public void setAccessByUser(final String entityId, final String accessByUser) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		if (entityId==null || entityId.length()<1) {
			
			// Don't set value.  This is a project preference.
			log.debug("PCP AccessByUser preference - empty/null entity value passed");
			return;
		}
		try {
			log.debug("PCP AccessByUser preference - Setting value for entity=" + entityId);
			this.set(SCOPE, entityId, accessByUser, ACCESS_BY_USER);
		} catch (UnknownToolId | InvalidPreferenceName e) {
			log.error("Exception setting pipeline control panel AccessByUser preference", e);
		}
	}
	
}
