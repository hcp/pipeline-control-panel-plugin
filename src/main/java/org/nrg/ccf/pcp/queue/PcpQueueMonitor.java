package org.nrg.ccf.pcp.queue;

import java.util.List;

import org.nrg.ccf.pcp.dto.PcpProjectPipeline;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;

public class PcpQueueMonitor {
	
	private static long lastId = 0;
	private long id = 0;
	private List<PcpCondensedStatusI> submitList;
	
	public PcpQueueMonitor(List<PcpCondensedStatusI> submitList) {
		this.id = getNewId();
		this.submitList = submitList;
	}
	
	public long getId() {
		return this.id;
	}
	
	public List<PcpCondensedStatusI> getSubmitList() {
		return submitList;
	}
	
	public PcpProjectPipeline getPcpProjectPipeline() {
		if (submitList.size()==0) {
			return null;
		}
		final PcpCondensedStatusI status = submitList.get(0);
		final PcpProjectPipeline pcpProjectPipeline = new PcpProjectPipeline(status.getProject(), status.getPipeline());
		return pcpProjectPipeline;
	}

	public boolean mayProceed() {
		return PcpSubmitQueue.mayProceed(this);
	}

	private synchronized long getNewId() {
		return lastId++;
	}

	public void notifyFinished() {
		PcpSubmitQueue.finished(this);
	}

	public boolean isCancelled() {
		return PcpSubmitQueue.isCancelled(this);
	}

}
