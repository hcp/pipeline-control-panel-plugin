package org.nrg.ccf.pcp.scheduler;

import org.nrg.ccf.pcp.preferences.PcpPreferences;
import org.nrg.ccf.pcp.scheduler.tasks.PcpStatusUpdate;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.ccf.pcp.utils.PcpConfigUtils;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.config.TriggerTask;
import org.springframework.scheduling.support.CronTrigger;

//import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableScheduling
//@Slf4j
public class PcpScheduler {
	
    @Bean
    public TriggerTask statusUpdateTrigger(final PcpStatusUpdateService statusUpdateService, final PcpPreferences pcpPreferences,
    		final PcpConfigUtils configUtils, final XnatTaskService taskService, UserManagementServiceI userManagementService,
    		JdbcTemplate jdbcTemplate) {
        return new TriggerTask(new PcpStatusUpdate(statusUpdateService, pcpPreferences, configUtils,
        		taskService, userManagementService, jdbcTemplate), new CronTrigger("0 */15 * * * *"));
    }

}
