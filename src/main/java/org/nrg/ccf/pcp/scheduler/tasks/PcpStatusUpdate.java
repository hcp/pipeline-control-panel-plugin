package org.nrg.ccf.pcp.scheduler.tasks;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.dto.ProjectQueryResults;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.exception.PcpUpdateRunningException;
import org.nrg.ccf.pcp.preferences.PcpPreferences;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.ccf.pcp.utils.PcpConfigUtils;
import org.nrg.config.entities.Configuration;
import org.nrg.framework.task.XnatTask;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.task.AbstractXnatTask;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.temporal.ChronoUnit;

import lombok.extern.slf4j.Slf4j;

@XnatTask(taskId = "PcpStatusUpdate", description = "Pipeline Control Panel Status Update Task", 
	defaultExecutionResolver = "SingleNodeExecutionResolver", executionResolverConfigurable = true)
@Slf4j
public class PcpStatusUpdate extends AbstractXnatTask implements Runnable {
	
	private final Map<String,UserI> userCache = new HashMap<>();
	private static int _currentlyRunning = 0;
	private final PcpStatusUpdateService _statusUpdateService;
	private final PcpPreferences _pcpPreferences;
	private final UserManagementServiceI _userManagementService;
	private PcpConfigUtils _configUtils;
	final RuntimeMXBean _mxBean = ManagementFactory.getRuntimeMXBean();
	private final JdbcTemplate _jdbcTemplate;
	
	final static String PROJECT_QUERY_SQL = "SELECT MIN(timestamp) as timestamp,project,pipeline " +
			" FROM xhbm_pcp_status_entity " +
			" WHERE project IN (@@PROJECTLIST@@) and status != 'REMOVED' " +
			" GROUP BY project,pipeline " +
			" ORDER BY timestamp,project,pipeline " 
	  ;

	public PcpStatusUpdate(final PcpStatusUpdateService statusUpdateService, final PcpPreferences pcpPreferences, final PcpConfigUtils configUtils,
			XnatTaskService taskService, UserManagementServiceI userManagementService, JdbcTemplate jdbcTemplate) {
		super(taskService);
		log.debug("Instantiate PcpStatusUpdate");
		_statusUpdateService = statusUpdateService;
		_pcpPreferences = pcpPreferences;
		_configUtils = configUtils;
		_userManagementService = userManagementService;
		_jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public void run() {
		log.debug("Run PcpStatusUpdate");
		if (_statusUpdateService == null) {
			log.error("Cannot run PCP status update.  Null value for PcpStatusUpdateService");
			return;
		}
		if (!shouldRunTask()) {
			log.info("PCP Status Update is not configured to run on this node.  Skipping.");
			return;
		}
		if (_currentlyRunning>=3) {
			log.info("Three PCP Status Update processes are already running.  This one will be skipped.");
			return;
		}
		try  {
			_currentlyRunning++;
			// CCF-326:  Let's not let this run if the web server has recently been started.  There were instances
			// where initial runs caused the status values to be set to REMOVED.  Presumably this can run when the 
			// site is only partially functional.
			final Long upTime = (_mxBean!=null) ? _mxBean.getUptime() : Long.MAX_VALUE;
			if (upTime<PcpConfigConstants.UP_TIME_WAIT) {
				log.info("Skipping PCP status update due to recent web server restart (UPTIME=" + upTime + "ms).");
				return;
			}
			
			List<String> projects = _pcpPreferences.getPcpEnabledProjects();
			StringBuilder sb = new StringBuilder();
			final Iterator<String> i = projects.iterator();
			while (i.hasNext()) {
				sb.append("'").append(i.next()).append("'");
				if (i.hasNext()) {
					sb.append(",");
				}
			}
			final List<ProjectQueryResults> queryResults = _jdbcTemplate.query(PROJECT_QUERY_SQL.replace("@@PROJECTLIST@@", sb.toString()), 
					new BeanPropertyRowMapper<ProjectQueryResults>(ProjectQueryResults.class));
			if (queryResults == null || queryResults.isEmpty()) {
				return;
			}
			for (final ProjectQueryResults results : queryResults) {
				final String projectId = results.getProject();
				final Configuration conf = _configUtils.getProjectConfig(projectId);
				if (conf==null) {
					log.error("ERROR:  Could not obtain configuration for " + PcpConfigConstants.CONFIG_TOOL + ".");
					return;
				}
		    	final PcpConfigInfo config = getConfig(results, projectId);
		    	if (config==null || config.getPipeline()==null) {
		    		continue;
		    	}
		    	final String currPipeline = config.getPipeline();
		    	if (runThisTime(projectId, config, results.getTimestamp())) {
		    		try {
		    			log.info("Running update status per configuration (PROJECT=" + projectId + ", PIPELINE=" + currPipeline +
		    					", UPDATE_FREQUENCY=" + config.getUpdateFrequency() + ").");
		    			_statusUpdateService.updateProjectStatus(projectId, currPipeline, getProjectUser(projectId), true);
		    		} catch (PcpComponentSetException e) {
		    			log.error("ERROR:  PCP project status could not be updated (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + ").");
		    		} catch (PcpUpdateRunningException e) {
		    			log.warn("Warning:  PCP update process already running - skipping this run (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + ").");
		    		} catch (Exception e) {
		    			log.error("ERROR:  Unexpected exception (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + ").");
					}
				} else {
		    		log.info("Skipping status update run per configuration (PROJECT=" + projectId + ", PIPELINE=" + currPipeline +
		    				", UPDATE_FREQUENCY=" + config.getUpdateFrequency() + ").");
				}
			}
		} finally {
			_currentlyRunning--;
		}
	}

	private PcpConfigInfo getConfig(ProjectQueryResults results, String projectId) {
		final List<PcpConfigInfo> configList = _configUtils.getProjectPcpConfigInfos(projectId);
		for (PcpConfigInfo config : configList) {
			if (config.getPipeline().equals(results.getPipeline())) {
				return config;
			}
		}
		return null;
	}

	private UserI getProjectUser(String projectId) {
		if (userCache.containsKey(projectId)) {
			return userCache.get(projectId);
		}
	    XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projectId, AdminUtils.getAdminUser(), false);
	    try {
			ArrayList<String> ownerList = proj.getOwners();
			for (final String owner : ownerList) {
				UserI user = _userManagementService.getUser(owner);
				if (user.isEnabled() && Roles.isSiteAdmin(user)) {
					return user;
				}
			}
			if (ownerList.size()>0) {
				return _userManagementService.getUser(ownerList.get(0));
			}
		} catch (Exception e) {
			// Do nothing
		}
		return AdminUtils.getAdminUser();
	}

	private boolean runThisTime(final String projectId, final PcpConfigInfo config, Date timestamp) {
	    final String currPipeline = config.getPipeline();
		log.info("StatusUpdate:  checking update status - (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + 
				", FREQUENCY=" + config.getUpdateFrequency() + ", TIMESTAMP=" + timestamp + ")" );
	    final Date currentTime = new Date();
	    boolean doRun = false;
		switch(config.getUpdateFrequency()) {
				case PcpConfigConstants.FREQUENCY_DISABLE_CACHING:
					// Doesn't need to be run by the scheduler.  Will be run each time status is requested.
					doRun=false;
					break;
				case PcpConfigConstants.FREQUENCY_15_MINUTES:
					if (ChronoUnit.MINUTES.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 15) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_30_MINUTES:
					if (ChronoUnit.MINUTES.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 30) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_HOURLY:
					if (ChronoUnit.HOURS.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 1) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_3_HOURS:
					if (ChronoUnit.HOURS.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 3) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_6_HOURS:
					if (ChronoUnit.HOURS.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 6) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_12_HOURS:
					if (ChronoUnit.HOURS.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 12) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_DAILY:
					if (ChronoUnit.HOURS.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 24) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_WEEKLY:
					if (ChronoUnit.DAYS.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 7) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_MONTHLY:
					if (ChronoUnit.MONTHS.between(getLocalDateTimeFromDate(timestamp), getLocalDateTimeFromDate(currentTime)) >= 1) {
						doRun=true;
					}
					break;
				case PcpConfigConstants.FREQUENCY_NO_UPDATE:
					doRun=false;
					break;
				default:
					log.warn("Invalid value for PCP status update frequency (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + ").");
					doRun=true;
					break;
		}
		return doRun;
	}
	
	private LocalDateTime getLocalDateTimeFromDate(Date date){

			   return LocalDateTime.from(Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()));

	}

}
