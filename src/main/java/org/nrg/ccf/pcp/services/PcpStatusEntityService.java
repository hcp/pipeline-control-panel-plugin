package org.nrg.ccf.pcp.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.nrg.ccf.pcp.dao.PcpStatusEntityDAO;
import org.nrg.ccf.pcp.dto.ExecutionGroup;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PcpStatusEntityService extends AbstractHibernateEntityService<PcpStatusEntity, PcpStatusEntityDAO> implements BaseHibernateService<PcpStatusEntity>  {

	@Transactional
	public List<PcpStatusEntity> getProjectStatus(String projectId) {
		return getDao().getProjectStatus(projectId);
	}

	@Transactional
	public List<PcpStatusEntity> getProjectPipelineStatus(String projectId, String pipelineId) {
		return getDao().getProjectPipelineStatus(projectId, pipelineId);
	}

	@Transactional
	public List<PcpStatusEntity> getStatusEntities(String projectId, String pipelineId, String entityIdOrLabel) {
		return getDao().getStatusEntities(projectId, pipelineId, entityIdOrLabel);
	}

	@Transactional
	public PcpStatusEntity getStatusEntity(String projectId, String pipelineId, ExecutionGroup executionGroup) {
		return getDao().getStatusEntity(projectId, pipelineId, executionGroup);
	}

	@Transactional
	public PcpStatusEntity getStatusEntity(PcpCondensedStatusI entity) {
		if (entity instanceof PcpStatusEntity) {
			return (PcpStatusEntity)entity;
		} else {
			return getDao().getStatusEntity(entity);
		}
	}

	@Transactional
	public Collection<PcpCondensedStatusI> getProjectPipelineCondensedStatus(String projectId, String pipelineId) {
		final List<PcpCondensedStatusI> returnList = new ArrayList<>();
		final List<PcpStatusEntity> fullStatusList = getDao().getProjectPipelineStatus(projectId, pipelineId);
		for (final PcpStatusEntity entity : fullStatusList) {
			returnList.add(entity.getCondensedStatus());
		}
		return returnList;
		
	}

	@Transactional
	public List<PcpCondensedStatusI> getCondensedStatusEntities(String projectId, 
			String pipelineId, String entityIdOrLabel) {
		final List<PcpCondensedStatusI> returnList = new ArrayList<>();
		final List<PcpStatusEntity> fullStatusList = getDao().getStatusEntities(projectId, pipelineId, entityIdOrLabel);
		for (final PcpStatusEntity entity : fullStatusList) {
			returnList.add(entity.getCondensedStatus());
		}
		return returnList;
	}

	@Transactional
	public boolean hasBeenModified(PcpStatusEntity statusEntity, Date retrieveTime) {
		final PcpStatusEntity entity = getDao().getStatusEntity(statusEntity);
		return entity.getTimestamp().after(retrieveTime);
	}

	@Transactional
	public List<PcpStatusEntity> getAllSubmittedOrRunning() {
		return getDao().getAllSubmittedOrRunning();
	}
	 
}