package org.nrg.ccf.pcp.services;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.dto.ComponentSet;
import org.nrg.ccf.pcp.dto.ExecutionGroup;
import org.nrg.ccf.pcp.dto.PcpProjectPipelineSubgroupEntity;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.exception.PcpEntityNotFoundException;
import org.nrg.ccf.pcp.exception.PcpUpdateRunningException;
import org.nrg.ccf.pcp.inter.*;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class PcpStatusUpdateService {

	private final PcpStatusEntityService _statusEntityService;
	private final PcpUtils _pcpUtils;
	private static ConcurrentHashMap<String, CopyOnWriteArrayList<String>> _cacheMap = new ConcurrentHashMap<>();
	private static final Logger _logger = LoggerFactory.getLogger(PcpStatusUpdateService.class);

	@Autowired
	public PcpStatusUpdateService(final PcpStatusEntityService statusEntityService, final PcpUtils pcpUtils) {
		super();
		_statusEntityService = statusEntityService;
		_pcpUtils = pcpUtils;
	}

	public class UpdateRunnable implements Runnable {

		private List<PcpStatusEntity> statusEntities;
		private UserI user;
		private ComponentSet componentSet;
		private Integer completed = 0;
		private boolean sendEmail;
		private String projectId;
		private String pipelineId;
		private Boolean _limitedRefresh;

		public UpdateRunnable(String projectId, String pipelineId, List<PcpStatusEntity> statusEntities, UserI user,
				ComponentSet componentSet, Boolean limitedRefresh) {
			super();
			this.projectId = projectId;
			this.pipelineId = pipelineId;
			this.statusEntities = statusEntities;
			this.user = user;
			this.componentSet = componentSet;
			this._limitedRefresh = limitedRefresh;
		}

		@Override
		public void run() {
			try {
				completed = 0;
				_logger.debug("Start time - " + new Date());
				for (final PcpStatusEntity statusEntity : statusEntities) {
					try {
						refreshStatusEntityCheckAndValidate(statusEntity, user, componentSet, _limitedRefresh);
					} catch (PcpComponentSetException e) {
						_logger.error("ComponentSet exception - updateEntityStatus - " + statusEntity);
					} catch (Throwable t) {
						_logger.error("PcpStatusUpdateService - ProcessingException - " + statusEntity + " - " + t);
						_logger.error(ExceptionUtils.getFullStackTrace(t));
					}
					completed++;
				}
				if (sendEmail) {
					StringBuilder sb = new StringBuilder();
					sb.append("<h3>PCP Status Update Refresh Completed (PROJECT=" + projectId + ", PIPELINE="
							+ pipelineId + ")</h3><br>");
					sb.append(TurbineUtils.GetFullServerPath());
					sb.append("/app/template/Pcp.vm?project=" + projectId + "&pipeline=" + pipelineId);
					_logger.debug("Sending e-mail - " + new Date());
					AdminUtils.sendUserHTMLEmail("PCP Status Update Refresh Completed", sb.toString(), false,
							new String[] { user.getEmail() });
				}
				_logger.debug("Thread run complete - " + getPctComplete());
			} finally {
				setNotRunning(projectId, pipelineId);
			}
		}

		private float getPctComplete() {
			float hasCompleted = completed.floatValue();
			return ((hasCompleted >= 1) ? hasCompleted : 1) / (float) (statusEntities.size());
		}

		public void setSendEmail(boolean sendEmail) {
			this.sendEmail = sendEmail;
		}

	}

	public int updateProjectStatus(final String projectId, final String pipelineId, final UserI user,
			final Boolean dontWait, final Boolean emailForEarlyReturn, final Boolean limitedRefresh)
			throws PcpComponentSetException, PcpUpdateRunningException {
		return updateProjectStatus(projectId, pipelineId, user, null, dontWait, emailForEarlyReturn, limitedRefresh);
	}

	public int updateProjectStatus(final String projectId, final String pipelineId, final UserI user,
			List<PcpCondensedStatusI> entityList, final Boolean dontWait, final Boolean emailForEarlyReturn,
			final Boolean limitedRefresh) throws PcpComponentSetException, PcpUpdateRunningException {
		if (isRunningIfNotSetToRunning(projectId, pipelineId)) {
			throw new PcpUpdateRunningException("Project status update already running for this pipeline");
		}
		try {
			final ComponentSet componentSet = _pcpUtils.getComponentSet(projectId, pipelineId);
			final PipelineSelectorI selector = componentSet.getSelector();
			final List<PcpStatusEntity> statusEntities = selector.getStatusEntities(_statusEntityService, projectId,
					pipelineId, user);
			if (entityList != null) {
				final List<PcpProjectPipelineSubgroupEntity> ppseList = new ArrayList<>();
				for (final PcpCondensedStatusI status : entityList) {
					ppseList.add(new PcpProjectPipelineSubgroupEntity(status.getProject(), status.getPipeline(),
							status.getSubGroup(), status.getEntityId()));
				}
				final Iterator<PcpStatusEntity> i = statusEntities.iterator();
				while (i.hasNext()) {
					final PcpStatusEntity entity = i.next();
					if (!ppseList.contains(entity.getProjectPipelineSubgroupEntity())) {
						i.remove();
					}
				}
			}
			Collections.sort(statusEntities);
			final UpdateRunnable runnable = new UpdateRunnable(projectId, pipelineId, statusEntities, user,
					componentSet, limitedRefresh);
			final ExecutorService executor = PcpExecService.getSingleUseExecutorService();
			executor.execute(runnable);
			executor.shutdown();
			if (!dontWait) {
				try {
					executor.awaitTermination(1, TimeUnit.DAYS);
				} catch (InterruptedException e) {
					setNotRunning(projectId, pipelineId);
					_logger.warn("UpdateRunnable thread interrupted");
				}
			} else {
				try {
					_logger.debug("Thread join - " + new Date());
					boolean didTimeout = executor.awaitTermination(10, TimeUnit.SECONDS);
					_logger.debug("Thread join ended - " + new Date());
					if (!didTimeout) {
						final float pctComplete = runnable.getPctComplete();
						if (pctComplete < .20) {
							runnable.setSendEmail(emailForEarlyReturn);
							// Seems to run faster towards the end.
							final int estComplete = (int) ((10 / pctComplete) - 10);
							_logger.debug("Completion percent - " + pctComplete + ", Estimated completion seconds - "
									+ estComplete);
							return estComplete;
						} else {
							_logger.debug("Thread rejoined - " + new Date());
							executor.awaitTermination(1, TimeUnit.DAYS);
						}
					}
				} catch (InterruptedException e) {
					setNotRunning(projectId, pipelineId);
					_logger.warn("UpdateRunnable thread interrupted");
				}
			}
			return -1;
		} catch (Exception e) {
			setNotRunning(projectId, pipelineId);
			throw e;
		}
	}

	public void updateProjectStatus(String projectId, String pipelineId, UserI user, final Boolean limitedRefresh)
			throws PcpComponentSetException, PcpUpdateRunningException {
		updateProjectStatus(projectId, pipelineId, user, false, false, limitedRefresh);
	}

	private synchronized boolean isRunningIfNotSetToRunning(String projectId, String pipelineId) {
		_cacheMap.putIfAbsent(projectId, new CopyOnWriteArrayList<String>());
		if (_cacheMap.get(projectId).contains(pipelineId)) {
			return true;
		} else {
			_cacheMap.get(projectId).add(pipelineId);
			return false;
		}
	}

	private synchronized void setNotRunning(String projectId, String pipelineId) {
		if (_cacheMap.containsKey(projectId)) {
			_cacheMap.get(projectId).remove(pipelineId);
		}
	}

	public void refreshStatusEntityCheckAndValidate(PcpStatusEntity statusEntity, UserI user, boolean limitedRefresh)
			throws PcpComponentSetException {
		final ComponentSet componentSet = _pcpUtils.getComponentSet(statusEntity.getProject(),
				statusEntity.getPipeline());
		refreshStatusEntityCheckAndValidate(statusEntity, user, componentSet, limitedRefresh);
	}

	private void refreshStatusEntityCheckAndValidate(PcpStatusEntity statusEntity, UserI user,
			ComponentSet componentSet, boolean limitedRefresh) throws PcpComponentSetException {
		final PipelinePrereqCheckerI prereqChecker = componentSet.getPrereqChecker();
		final PipelineStatusUpdaterI statusUpdater = componentSet.getStatusUpdater();
		final PipelineValidatorI validator = componentSet.getValidator();
		_statusEntityService.refresh(statusEntity);
		final String currentStatus = statusEntity.getStatus();
		// If entity is currently submitted or running, we'll let that process do the
		// validation.
		if (currentStatus == null || !(currentStatus.equals(PcpConstants.PcpStatus.RUNNING.toString())
				|| currentStatus.equals(PcpConstants.PcpStatus.SUBMITTED.toString()))) {
			if (limitedRefresh && statusEntity.getValidated() && statusEntity.getPrereqs() && 
					// TODO:  The number of days here should be configurable
					ChronoUnit.DAYS.between(getLocalDateFromDate(statusEntity.getTimestamp()), 
							getLocalDateFromDate(new Date(System.currentTimeMillis()))) < 7) {
				// skip check;
				return;
			}
			prereqChecker.checkPrereqs(statusEntity, user);
			validator.validate(statusEntity, user);
			// Update status last. Its calculation is based on current results.
			statusUpdater.updateStatus(statusEntity, user);
			_statusEntityService.update(statusEntity);
		}
	}

	public void runValidation(String projectId, String pipelineId, ExecutionGroup executionGroup, UserI sessionUser,
			ComponentSet componentSet) throws PcpEntityNotFoundException {
		final PipelineValidatorI validator = componentSet.getValidator();
		final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(projectId, pipelineId,
				executionGroup);
		if (statusEntity == null) {
			throw new PcpEntityNotFoundException();
		}
		validator.validate(statusEntity, sessionUser);
		_statusEntityService.update(statusEntity);
	}

	public void setStatus(String projectId, String pipelineId, ExecutionGroup executionGroup, UserI sessionUser,
			PcpConstants.PcpStatus status) throws PcpEntityNotFoundException {
		final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(projectId, pipelineId,
				executionGroup);
		if (statusEntity == null) {
			throw new PcpEntityNotFoundException();
		}
		statusEntity.setStatus(status);
		statusEntity.setStatusTime(new Date());
		_statusEntityService.update(statusEntity);
	}
	
	private LocalDate getLocalDateFromDate(Date date){
		   return LocalDate.from(Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()));
	}

}
