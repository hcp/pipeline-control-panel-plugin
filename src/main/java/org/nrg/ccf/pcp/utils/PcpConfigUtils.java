package org.nrg.ccf.pcp.utils;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class PcpConfigUtils {
	
	private final ConfigService _configService;
	private final Gson _gson = new Gson();

	@Autowired
	public PcpConfigUtils(ConfigService configService) {
		super();
		_configService = configService;
	}
	
	public Configuration getProjectConfig(final String projectId) {
		return _configService.getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, Scope.Project, projectId);
	}
	
	public List<PcpConfigInfo> getProjectPcpConfigInfos(final String projectId) {
		final Configuration config = getProjectConfig(projectId);
    	final List<PcpConfigInfo> returnVal;
    	if (config != null) {
    		returnVal = _gson.fromJson(config.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
    	} else {
    		returnVal = new ArrayList<>();
    	}
    	return returnVal;
    	
	}
	
	public PcpConfigInfo getProjectPcpConfigInfo(final String projectId, final String pipeline) {
		for (final PcpConfigInfo config : getProjectPcpConfigInfos(projectId)) {
			if (pipeline.equals(config.getPipeline())) {
				return config;
			}
		}
		return null;
	}
	
	public boolean hasProjectPcpConfigInfo(final String projectId, final String pipeline) {
		for (final PcpConfigInfo config : getProjectPcpConfigInfos(projectId)) {
			if (pipeline.equals(config.getPipeline())) {
				return true;
			}
		}
		return false;
	}
	
}
