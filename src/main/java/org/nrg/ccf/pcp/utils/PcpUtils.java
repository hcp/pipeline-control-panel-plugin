package org.nrg.ccf.pcp.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.constants.PcpConstants.AccessLevel;
import org.nrg.ccf.pcp.dto.ComponentSet;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.dto.PcpSubmitItem;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.inter.*;
import org.nrg.ccf.pcp.preferences.PcpPreferences;
import org.nrg.config.entities.Configuration;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.preferences.SiteConfigPreferences;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.services.AliasTokenService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.node.services.XnatNodeInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PcpUtils {
	
	public static final Map<String,Map<String,ComponentSet>> componentSetMap = new HashMap<>();
	private final PcpConfigUtils _configUtils;
	private final Gson _gson = new Gson();
	private final PcpPreferences _pcpPreferences;

	@Autowired
	public PcpUtils(PcpConfigUtils configUtils, PcpPreferences pcpPreferences) {
		super();
		_configUtils = configUtils;
		_pcpPreferences = pcpPreferences;
	}

	public ComponentSet getComponentSet(final String projectId, final String pipelineId) throws PcpComponentSetException {
		if (!componentSetMap.containsKey(projectId)) {
			final Map<String,ComponentSet> projectMap = new HashMap<>();
			componentSetMap.put(projectId, projectMap);
		}
		final Map<String,ComponentSet> projectMap = componentSetMap.get(projectId); 
		if (!projectMap.containsKey(pipelineId)) {
			final ComponentSet pipelineSet = getNewComponentSet(projectId, pipelineId);
			return pipelineSet;
		} else {
			return projectMap.get(pipelineId);
		}
	}

	public ComponentSet getComponentSet(PcpCondensedStatusI entity) throws PcpComponentSetException {
		return getComponentSet(entity.getProject(), entity.getPipeline());
	}

	private ComponentSet getNewComponentSet(final String projectId, final String pipelineId) throws PcpComponentSetException {
		final Configuration conf = _configUtils.getProjectConfig(projectId);
		if (conf==null) {
			throw new PcpComponentSetException("Could not return component set for project pipeline.  No project configuration found.");
		}
    	final List<PcpConfigInfo> confList = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
    	for (final PcpConfigInfo configInfo : confList) {
    		if (configInfo.getPipeline().equals(pipelineId)) {
    			final String selectorStr = configInfo.getSelector();
    			final String submitterStr = configInfo.getSubmitter();
    			final String prereqCheckerStr = configInfo.getPrereqChecker();
    			final String statusUpdaterStr = configInfo.getStatusUpdater();
    			final String execManagerStr = configInfo.getExecManager();
    			final String validatorStr = configInfo.getValidator();
    			if (selectorStr==null || submitterStr==null || prereqCheckerStr==null || statusUpdaterStr==null ||
    					execManagerStr==null || validatorStr==null) {
    				throw new PcpComponentSetException("Configuration returned null value for one or more components.");
    			}
				try {
					final PipelineSelectorI selector = (PipelineSelectorI)Class.forName(selectorStr).newInstance();
					final PipelineSubmitterI submitter = (PipelineSubmitterI)Class.forName(submitterStr).newInstance();
					final PipelinePrereqCheckerI prereqChecker = (PipelinePrereqCheckerI)Class.forName(prereqCheckerStr).newInstance();
					final PipelineStatusUpdaterI statusUpdater = (PipelineStatusUpdaterI)Class.forName(statusUpdaterStr).newInstance();
					final PipelineExecManagerI execManager = (PipelineExecManagerI)Class.forName(execManagerStr).newInstance();
					final PipelineValidatorI validator = (PipelineValidatorI)Class.forName(validatorStr).newInstance();
					return new ComponentSet(execManager, selector, submitter, statusUpdater, prereqChecker, validator);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
    				throw new PcpComponentSetException("Exception thrown instantiating one or more components.", e);
				}
    		}
    	}
		throw new PcpComponentSetException("Could not return component set for project pipeline.  No matching pipeline configuration found.");
	}
	
	public void submitJobs(String currentProject, String currentPipeline, PcpSubmitItem submitItem, List<PcpCondensedStatusI> statusList,
			Map<String,String> parameters, UserI user, Boolean forceProcessLocally) throws PcpComponentSetException {
		final ComponentSet componentSet = getComponentSet(currentProject, currentPipeline);
		final PipelineExecManagerI execManager = componentSet.getExecManager();
		final PipelineSubmitterI submitter = componentSet.getSubmitter();
		final PipelineValidatorI validator = componentSet.getValidator();
		final XnatTaskService taskService = XDAT.getContextService().getBean(XnatTaskService.class);
		final SiteConfigPreferences preferences = XDAT.getContextService().getBean(SiteConfigPreferences.class);
		final XnatNodeInfoService nodeInfoService = XDAT.getContextService().getBean(XnatNodeInfoService.class);
		final AliasTokenService tokenService = XDAT.getContextService().getBean(AliasTokenService.class);
		final PcpSubmitTask submitTask = new PcpSubmitTask(
				currentProject, currentPipeline, execManager, submitter, validator, submitItem, statusList, parameters, user,
				taskService, preferences, nodeInfoService, tokenService, forceProcessLocally);
		submitTask.submit();
	}

	public List<String> getSubmitParametersYaml(String projectId, String pipelineId) throws PcpComponentSetException {
		final ComponentSet componentSet = getComponentSet(projectId, pipelineId);
		final PipelineSubmitterI submitter = componentSet.getSubmitter();
		return submitter.getParametersYaml(projectId, pipelineId);
	}

	public List<String> getConfiguredProjects(final String pipeline, final UserI user) throws Exception {
		final List<String> returnList = new ArrayList<>();
		final ArrayList<XnatProjectdata> projects = XnatProjectdata.getAllXnatProjectdatas(user, false);
		final List<String> pcpEnabledProjects = _pcpPreferences.getPcpEnabledProjects();
		for (final XnatProjectdata project : projects) {
			final String projectId = project.getId();
			if (!Permissions.canEdit(user, project) || !pcpEnabledProjects.contains(project.getId())) {
				continue;
			}
			if (pipeline == null) {
				returnList.add(projectId);
			} else {
				final List<PcpConfigInfo> projectConfigs = _configUtils.getProjectPcpConfigInfos(projectId);
				for (final PcpConfigInfo projectConfig : projectConfigs) {
					if (projectConfig.getPipeline().equals(pipeline)) {
						returnList.add(projectId);
						break;
					}
				}
			}
		}
		return returnList;
	}

	public Boolean userHasPcpAccess(UserI user, String projectId) {
		final Boolean isEnabled = _pcpPreferences.getPcpEnabled(projectId);
		if (isEnabled == null || !isEnabled) {
			return false;
		}
		final String accessByRoleStr = _pcpPreferences.getAccessByRole(projectId);
		final String accessByRole = (accessByRoleStr != null) ? accessByRoleStr : AccessLevel.ADMIN.name();
		final String accessUsersStr = _pcpPreferences.getAccessByUser(projectId);
		final String[] accessUsers = (accessUsersStr != null) ? 
				accessUsersStr.split(",") : new String[] {};
		try {
			if (Arrays.asList(accessUsers).contains(user.getUsername()) && Permissions.canReadProject(user,projectId) ) {
				return true;
			}
		} catch (Exception e) {
			// Just log and continue
			log.error("Exception thrown checking project permissions:  " + e.toString());
		}
		if (accessByRole.equals(AccessLevel.OWNER.name())) {
			return Permissions.canDeleteProject(user, projectId);
		} else if (accessByRole.equals(AccessLevel.MEMBER.name())) {
			return Permissions.canEditProject(user, projectId);
		}
		return Roles.isSiteAdmin(user);
	}
	
}

