package org.nrg.ccf.pcp.xapi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.nrg.ccf.pcp.anno.PipelineExecManager;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.anno.PipelineStatusUpdater;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.ComponentInfo;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.dto.PcpSettingsInfo;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.preferences.PcpPreferences;
import org.nrg.ccf.pcp.utils.PcpConfigUtils;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.constants.Scope;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@XapiRestController
@Api(description = "Pipeline Control Panel Configuration API")
public class PcpConfigApi extends AbstractXapiRestController {

	private final ConfigService _configService;
	private PcpConfigUtils _pcpConfigUtils;
	private final PcpPreferences _preferences;
	private final Gson _gson = new Gson();
   	private final Map<String,List<String>> _componentMap = new HashMap<>();
   	private final List<String> _allComponents = new ArrayList<>();
   	private final Map<String,List<String>> _componentConfig = new HashMap<>();
   	private ComponentInfo _componentInfo;
	private List<Resource> _resourceList;
	private PcpUtils _pcpUtils;
	
	private static final Logger _logger = LoggerFactory.getLogger(PcpConfigApi.class);

	@Autowired
	protected PcpConfigApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
								final ConfigService configService, final PcpPreferences preferences, 
								final PcpUtils pcpUtils, final PcpConfigUtils pcpConfigUtils) {
		super(userManagementService, roleHolder);
		_configService = configService;
		_preferences = preferences;
		_pcpUtils = pcpUtils;
		_pcpConfigUtils = pcpConfigUtils;
	}

	@ApiOperation(value = "Gets PCP project settings", notes = "Returns PCP project-level settings.", response = PcpSettingsInfo.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/{projectId}/settings"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<PcpSettingsInfo> getPcpProjectSettings(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		final PcpSettingsInfo settingsInfo = new PcpSettingsInfo();
    	final Boolean enabledVal = _preferences.getPcpEnabled(projectId);
    	final String accessByRole = _preferences.getAccessByRole(projectId);
    	final String accessByUser = _preferences.getAccessByUser(projectId);
    	if (enabledVal != null) {
    		settingsInfo.setPcpEnabled(enabledVal);
    	}
    	if (accessByRole != null) {
    		settingsInfo.setAccessByRole(accessByRole);
    	}
    	if (accessByUser != null) {
    		settingsInfo.setAccessByUser(accessByUser);
    	}
    	String test = _preferences.toString();
    	System.out.println(test);
    	return new ResponseEntity<>(settingsInfo,HttpStatus.OK);
    }
	
    @ApiOperation(value = "Sets PCP project settings", notes = "Sets PCP project-level settings.", response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/{projectId}/settings"}, restrictTo=AccessLevel.Owner, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> getPcpProjectSettings(@PathVariable("projectId") @ProjectId final String projectId, 
    			@RequestBody final Map<String, Object> config) throws NrgServiceException {
    	if (config.containsKey(PcpPreferences.PCP_ENABLED)) {
    		_preferences.setPcpEnabled(projectId, Boolean.valueOf(config.get(PcpPreferences.PCP_ENABLED).toString()));
    	}
    	if (config.containsKey(PcpPreferences.ACCESS_BY_ROLE)) {
    		_preferences.setAccessByRole(projectId, config.get(PcpPreferences.ACCESS_BY_ROLE).toString());
    	}
    	if (config.containsKey(PcpPreferences.ACCESS_BY_USER)) {
    		_preferences.setAccessByUser(projectId, config.get(PcpPreferences.ACCESS_BY_USER).toString());
    	}
    	String test = _preferences.toString();
    	System.out.println(test);
    	return new ResponseEntity<>(HttpStatus.OK);
    }
	
    @ApiOperation(value = "Has PCP Access?", notes = "Check PCP access for project.", response = Void.class)
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/{projectId}/hasAccess"}, restrictTo=AccessLevel.Authenticated, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Void> hasPcpAccess(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
    	if (_pcpUtils.userHasPcpAccess(this.getSessionUser(), projectId)) {
    		return new ResponseEntity<>(HttpStatus.OK);
    	}
   		return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
	
    /*
    @ApiOperation(value = "Gets PCP component map", notes = "Returns PCP component map.", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/componentMap"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String,List<String>>> getPcpAnnotatedClasses() throws NrgServiceException {
    	return new ResponseEntity<>(_componentMap,HttpStatus.OK);
    }
    */
	
    @ApiOperation(value = "Gets PCP component map", notes = "Returns PCP component map.", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/componentMap"}, restrictTo=AccessLevel.Authenticated, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ComponentInfo> getPcpAnnotatedClasses() throws NrgServiceException {
    	return new ResponseEntity<>(getComponentInfo(),HttpStatus.OK);
    }
	
    @ApiOperation(value = "Gets project changer projects", notes = "Returns PCP project changer project list.", response = List.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/projectChangerProjects"}, restrictTo=AccessLevel.Authenticated, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> getProjectChangerProjects(
    		@RequestParam(value = "pipeline", required = false) final String pipeline) throws NrgServiceException {
        final List<String> returnList = new ArrayList<>();
		try {
			returnList.addAll(_pcpUtils.getConfiguredProjects(pipeline, getSessionUser()));
			return new ResponseEntity<>(returnList,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(returnList,HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
    @ApiOperation(value = "Gets project PCP pipeline configurations", notes = "Returns project-level PCP pipeline configurations.", response = List.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/{projectId}/pipelines"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<PcpConfigInfo>> getPcpProjectPipelines(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
    	return new ResponseEntity<>(_pcpConfigUtils.getProjectPcpConfigInfos(projectId),HttpStatus.OK);
    }
	
    @ApiOperation(value = "Sets PCP project pipelines configuration", notes = "Sets PCP project-level pipelines configuration.", response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanelConfig/{projectId}/pipelines"}, restrictTo=AccessLevel.Owner, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setPcpProjectPipelines(@PathVariable("projectId") @ProjectId final String projectId, 
    			@RequestBody final List<Map<String, Object>> config) throws NrgServiceException {
    	final String configJson = _gson.toJson(config);
		_configService.replaceConfig(this.getSessionUser().getUsername(), "",  PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, configJson, Scope.Project, projectId);
		PcpApi.clearOnDemandCache(projectId);
    	return new ResponseEntity<>(HttpStatus.OK);
    }
	
    private void populateComponentMap() {
    	try {
    		_componentMap.put(PcpConfigConstants.UPDATE_FREQUENCY, PcpConfigConstants.UPDATE_FREQUENCY_OPTIONS);
    		populateComponentMap(PipelineSelector.PIPELINE_SELECTOR);
    		populateComponentMap(PipelineSubmitter.PIPELINE_SUBMITTER);
    		populateComponentMap(PipelineStatusUpdater.PIPELINE_STATUS_UPDATER);
    		populateComponentMap(PipelinePrereqChecker.PIPELINE_PREREQ_CHECKER);
    		populateComponentMap(PipelineValidator.PIPELINE_VALIDATOR);
    		populateComponentMap(PipelineExecManager.PIPELINE_EXEC_MANAGER);
    	} catch (IOException e) {
    		_logger.error("ERROR:  Exception thrown populating component map.", e);
    		
    	}
	}
	
    private void populateComponentConfig() {
    	for (final String compStr : _allComponents) {
    		try {
				final Class<?> compCl = Class.forName(compStr);
				if (ConfigurableComponentI.class.isAssignableFrom(compCl)) {
					final ConfigurableComponentI comp = (ConfigurableComponentI)compCl.newInstance();
					final List<String> configYaml = comp.getConfigurationYaml();
					_componentConfig.put(compStr, configYaml);
				}
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				_logger.error("ERROR:  Component class not found:  " + compStr);
			}
    	}
	}

	private void populateComponentMap(final String component) throws IOException {
		final List<String> componentList = new ArrayList<>();
		for (final Resource resource : getResourceList()) {
			final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
			if (!properties.containsKey(component)) {
				continue;
			}
			componentList.add(properties.getProperty(component.toString()));
		}
		_componentMap.put(component, componentList);
		_allComponents.addAll(componentList);
	}
    
	private List<Resource> getResourceList() throws IOException {
		return (_resourceList!=null) ? _resourceList : BasicXnatResourceLocator.getResources("classpath*:META-INF/xnat/pcp/*-pcp.properties");
	}
	
	private ComponentInfo getComponentInfo() {
		// TODO:  Currently we're doing this here because we're not autowiring the components.  If
		// we begin letting Spring manage the components, this can be done at controller initialization.
		//if (_componentInfo==null) {
			populateComponentMap();
			// Note:  This call must occur after the populateComponentMap call
			populateComponentConfig();
			_componentInfo = new ComponentInfo(_componentMap,_componentConfig);
		//}
		return _componentInfo;
	}

}
