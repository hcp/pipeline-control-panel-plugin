package org.nrg.ccf.pcp.xapi;

import org.nrg.ccf.pcp.dto.PcpSubmitStatus;
import org.nrg.ccf.pcp.dto.PcpSummaryStatus;
import org.nrg.ccf.pcp.exception.PcpJobNotFoundException;
import org.nrg.ccf.pcp.queue.PcpSubmitQueue;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "Pipeline Control Panel API")
public class PcpMonitorApi extends AbstractXapiRestController {

	@Autowired
	protected PcpMonitorApi(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}
	
	
	@ApiOperation(value = "Gets PCP project submit status", notes = "Returns PCP project submit status.",
			responseContainer = "List", response = PcpSummaryStatus.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "JobID doesn't exist in waiting state."), 
    	@ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pcpSubmitStatus"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<PcpSubmitStatus> getPcpSubmitStatus(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		return new ResponseEntity<>(PcpSubmitQueue.getPcpSubmitStatus(projectId),HttpStatus.OK);
	}
	
	@ApiOperation(value = "Cancels PCP waiting submit", notes = "Cancels PCP waiting submit.",
			responseContainer = "List", response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Job marked for cancellation.  Could take a couple minutes to show as cancelled."), 
    	@ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/cancelWaitingJob/{jobNum}"}, restrictTo=AccessLevel.Member,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> getPcpSubmittStatus(@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("jobNum") final Long jobNum) throws NrgServiceException {
		try {
			PcpSubmitQueue.cancel(projectId, jobNum);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (PcpJobNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
}
