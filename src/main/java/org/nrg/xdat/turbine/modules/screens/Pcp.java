package org.nrg.xdat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
public class Pcp extends SecureScreen {
	
	@Override
	public void doBuildTemplate(RunData data, Context context) {
		final PcpUtils pcpUtils = XDAT.getContextService().getBean(PcpUtils.class); 
		final UserI user = XDAT.getUserDetails();
        final String projectId = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("project",data));
        context.put("hasPcpAccess", pcpUtils.userHasPcpAccess(user, projectId));
	}
     
}
