
//# sourceURL=pipeline-control-panel-plugin/pipelineControlPanel.js

if (typeof CCF === 'undefined') {
  CCF = {};
}
if (typeof CCF.pcp === 'undefined') {
  CCF.pcp = {
    ACCESS_URL: serverRoot+"/xapi/pipelineControlPanelConfig/"+XNAT.data.context.project+"/hasAccess",
    project: XNAT.data.context.project,
    selectedSubgroups: undefined,
    currentSubgroups: undefined,
    BIG_TABLE_SIZE: 4000,
    ENTITY_GROUP_SIZE: 2000,
    groupMap: new Map(),
  };
}

CCF.pcp.initialize = function(projectId) {
  CCF.pcp.project = projectId
  $.ajax({
    type: "GET",
    url:CCF.pcp.ACCESS_URL,
    cache: false,
    async: true,
    context: this,
    dataType: 'json'
  }).done( function(data, textStatus, jqXHR) {
        $(".pcpLink").show();
  }).fail( function(data, textStatus, jqXHR) {
        $(".pcpLink").hide();
  });
}

CCF.pcp.renderPipelineControlPanel = function() {

  $(".crumb").hide();

  var getSearchParams = function(k){
     var p={};
     location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
     return k?p[k]:p;
  }

  CCF.pcp.project = getSearchParams("project")
  CCF.pcp.pipeline = getSearchParams("pipeline")

  var CONFIG_URL = serverRoot+"/xapi/pipelineControlPanelConfig/"+CCF.pcp.project+"/pipelines/";
  $.ajax({
    type: "GET",
    url:CONFIG_URL,
    cache: false,
    async: true,
    context: this,
    dataType: 'json'
  }).done( function(data, textStatus, jqXHR) {
        CCF.pcp.pipelineConfig = data;
        if (CCF.pcp.pipeline) {
         CCF.pcp.renderPipelineControls()
         CCF.pcp.renderPipelineTable()
        } else {
            CCF.pcp.renderOverviewControls()
            CCF.pcp.renderOverviewTable()
        }
        CCF.pcp.renderProjectChanger();
        CCF.pcp.renderPipelineChanger();
        $(".crumb").show();
  }).fail( function(data, textStatus, jqXHR) {
    console.log("ERROR:  Error returning Pipeline Control Panel pipeline configuration.");
  });

}

CCF.pcp.renderProjectChanger = function() {
  $(".project-changer").chosen({ disable_search_threshold: 7, 
				 search_contains: true,
				 placeholder_text_single: "Project:",
				 width: "150px"
				 });
  $(".project-changer").parent().find(".chosen-container-single .chosen-single").hide();
  $(".project-changer").parent().find(".chosen-container-single .chosen-single").css({ height: "26px", fontSize: "large"}); 
  $(".project-changer").parent().find(".chosen-drop ").css({ width: "200px", fontSize: "larger"}); 
  $(".project-changer").parent().find(".chosen-single ").css({ color: "#084fab"}); 
  $(".project-changer").parent().find(".chosen-single ").css({ color: "#084fab", border: "none", boxShadow: "none", backgroundImage: "none" }); 
  $(".project-changer").parent().find(".chosen-container-single .chosen-single").show();
  //$(".project-changer").parent().find(".chosen-single").find("b").css({ background: "none" }); 
  $(".project-changer").on('change', function(evt, params) {
  	var projectVal = $(".project-changer").val();
  	if (CCF.pcp.project !== projectVal) { 
  		window.location.replace(window.location.href.replace("project=" + CCF.pcp.project, "project=" + projectVal));
  		setTimeout(function() { $(".project-changer").val("").trigger("chosen:updated") }, 200);
  	} else {
  		$(".project-changer").val("").trigger("chosen:updated");
  	} 
  });
}

CCF.pcp.updateProjectChanger = function(changerPipeline) {
  var CHANGER_URL = serverRoot+"/xapi/pipelineControlPanelConfig/projectChangerProjects" + 
	((typeof changerPipeline !== 'undefined') ? "?pipeline=" + changerPipeline : "");
  $.ajax({
    type: "GET",
    url:CHANGER_URL,
    cache: false,
    async: true,
    context: this,
    dataType: 'json'
  }).done( function(data, textStatus, jqXHR) {
	var projectList = data;
	projectList.sort();
	$(".project-changer").prop("disabled", false);
	$(".project-changer").empty();
	$(".project-changer").append("<option></option>");
	$.each(projectList, function(inx,val) {
		$(".project-changer").append("<option>" + val + "</option>");
	});
	$(".project-changer").trigger("chosen:updated");
  }).fail( function(data, textStatus, jqXHR) {
     console.log("ERROR:  Error return projectChangerProjects.");
  });
}

CCF.pcp.renderPipelineChanger = function() {
  $(".pipeline-changer").chosen({ disable_search_threshold: 7, 
				 search_contains: true,
				 placeholder_text_single: "All Pipelines",
				 width: "250px"
				 });
  $(".pipeline-changer").parent().find(".chosen-container-single .chosen-single").hide();
  $(".pipeline-changer").parent().find(".chosen-container-single .chosen-single").css({ height: "26px", fontSize: "large"}); 
  $(".pipeline-changer").parent().find(".chosen-drop ").css({ width: "200px", fontSize: "larger"}); 
  $(".pipeline-changer").parent().find(".chosen-single").css({ color: "#084fab", border: "none", boxShadow: "none", backgroundImage: "none", background: "none" }); 
  $(".pipeline-changer").parent().find(".chosen-single").find("b").css({ background: "none" }); 
  $(".pipeline-changer").parent().find(".chosen-container-single .chosen-single").show();
  $(".pipeline-changer").on('change', function(evt, params) {
  	var pipelineVal = $(".pipeline-changer").val();
        var href = window.location.href;
  	if (!href.match(/^.*pipeline=/)) {
  		window.location.replace(href + "&pipeline=" + pipelineVal);
  	} else if (CCF.pcp.pipeline !== pipelineVal) { 
  		window.location.replace(href.replace("pipeline=" + CCF.pcp.pipeline, "pipeline=" + pipelineVal));
  	} 
  });
}

CCF.pcp.updatePipelineChanger = function(changerPipeline) {
  $(".pipeline-changer").html('');
  $(".pipeline-changer").append('<option></option>');
  var pConfig = CCF.pcp.pipelineConfig;
  pConfig.sort(function(a,b) {
		return a.pipeline.localeCompare(b.pipeline);
	}
  );
  for (var i=0; i<pConfig.length; i++) {
     var pipe = pConfig[i].pipeline;
     if (typeof pipe !== 'undefined') {
	    $(".pipeline-changer").append("<option>" + pipe + "</option>");
     }
  }
  if (typeof changerPipeline !== 'undefined') {
    $(".pipeline-changer").val(changerPipeline);
  }
  $(".pipeline-changer").prop("disabled", false);
  $(".pipeline-changer").trigger("chosen:updated");
}


CCF.pcp.renderBreadcrumbs = function() {
	 // Hide XNAT breadcrumbs and create custom
  $('a[id^="breadcrumb"]').hide()
  $('#project-link').text(CCF.pcp.project)
  $('#project-link').attr('href','/data/projects/'+CCF.pcp.project)
  $('#pcp-overview-link').attr('href','/app/template/Pcp.vm?project='+CCF.pcp.project)
}

CCF.pcp.renderOverviewControls = function() {
	var $pcpControls = $('#pcp-controls-container')
  $pcpControls.empty()

  CCF.pcp.renderBreadcrumbs()

  var refreshAction = "CCF.pcp.renderOverviewTable()"
  var refreshButton = '<button class="btn" onclick=' + refreshAction + '>Refresh</button>&nbsp;'
  var settingsButton = '<button class="btn" onclick="location.href=\'/app/template/Page.vm?view=project/settings&id=' + CCF.pcp.project + '\'">Settings</button>&nbsp;'
  $pcpControls.append(refreshButton)
  $pcpControls.append(settingsButton)
}

CCF.pcp.renderPipelineControls = function() {
	var $pcpControls = $('#pcp-controls-container')
	$pcpControls.empty()

	CCF.pcp.renderBreadcrumbs()

  //$('#pcp-title').text(CCF.pcp.pipeline)

  // Need to add tooltip to explain the difference once these are spawner elements
  var quickRefreshButton = '<button class="btn" onclick="CCF.pcp.renderPipelineTable(true)">Refresh</button>&nbsp;'
  //var fullRefreshButton = '<button class="btn" onclick="CCF.pcp.renderPipelineTable(false)">Update Cache</button>&nbsp;'
  var fullRefreshButton = '<button class="btn" onclick="CCF.pcp.renderStatusUpdate()">Update Cache</button>&nbsp;'
  var removeSelectedButton = '<button class="btn" onclick="CCF.pcp.removeSelected()">Remove Selected</button>&nbsp;'
  var resetStatusButton = '<button class="btn" onclick="CCF.pcp.resetStatusSelected()">Reset Status</button>&nbsp;'
  var infoReportButton = '<button class="btn" onclick="CCF.pcp.infoReportSelected()">Info Report</button>&nbsp;'
  var csvDownloadButton = '<button class="btn" onclick="CCF.pcp.csvDownloadSelected()">Summary CSV</button>&nbsp;'
  var subgroupFiltersButton = '<button id="subgoupFiltersButton" class="btn hidden" onclick="CCF.pcp.subgroupFiltersSelected()">Subgroup Filters</button>&nbsp;'
  var exportButton = '<button>Export</button>'
  var submitButton = '<button class="submit btn" onclick="CCF.pcp.selectPipelineToLaunch()">Launch Pipeline</button>'
  $pcpControls.append(quickRefreshButton)
  $pcpControls.append(fullRefreshButton)
  $pcpControls.append(removeSelectedButton)
  $pcpControls.append(resetStatusButton)
  $pcpControls.append(infoReportButton)
  $pcpControls.append(csvDownloadButton)
  $pcpControls.append(subgroupFiltersButton)
  $pcpControls.append(submitButton)
}

CCF.pcp.populateNewPipelineIfAny = function(tableData) {
  var CONFIG_URL = serverRoot+"/xapi/pipelineControlPanelConfig/"+CCF.pcp.project+"/pipelines/";
  $.ajax({
    type: "GET",
    url:CONFIG_URL,
    cache: false,
    async: true,
    context: this,
    dataType: 'json'
  }).done( function(data, textStatus, jqXHR) {
	$.each(data, function(inx,val) {
		var hasMatch = "N";
		$.each(tableData, function(inx2,val2) {
			if (val.pipeline === val2.pipeline) {
				hasMatch = "Y";
			}
		});
		if ( hasMatch === "N" ) {
			// Pipeline not represented in table.  Let's call status update so it starts building.
			var status_update_url = serverRoot + '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
			     '/pipeline/' + val.pipeline +
			     '/status?cached=false&condensed=false&dontWait=true&emailForEarlyReturn=false';
			
			XNAT.xhr.get({
			  	url: status_update_url,
				cache: false,
				async: true,
				context: this,
			}).done( function(data, textStatus, jqXHR) {
				// Do nothing
			})
		}
	});
  });
}

CCF.pcp.doOverviewTableRender = function(tableData) {

  XNAT.table.dataTable(tableData, {
     table: {
        id: 'pcp-overview',
        className: 'pcp-table'
     },
     columns: {
        pipeline: {
          label: 'Pipeline',
          filter: true,
          apply: function(pipeline) {
            // var actions = "CCF.pcp.renderPipelineControls('"+pipeline+"');CCF.pcp.renderPipelineTable()"
            // return '<a onclick="' + actions + '">' + pipeline + '</a>'
            return '<a href=/app/template/Pcp.vm?project='+CCF.pcp.project+'&pipeline='+pipeline+'>'+pipeline+'</a>'
          }
        },
        notReady: {
          label: 'Not Ready',
          sort: true
        },
        ready: {
          label: 'Ready',
          sort: true
        },
        submitted: {
          label: 'Submitted',
          sort: true
        },
        running: {
          label: 'Queued or Running',
          sort: true
        },
        issues: {
          label: 'Issues',
          sort: true
        },
        complete: {
          label: 'Complete',
          sort: true
        },
        total: {
          label: 'Total',
          sort: true
        }
     }
  }).render($('#pcp-table-container'))


}

CCF.pcp.renderOverviewTable = function() {
  $('#pcp-table-container').empty()
  $('#pcp-row-count-container').empty();

  if (typeof CCF.pcp.project === 'undefined' || CCF.pcp.project == "") {
     var queryParams = new URLSearchParams(window.location.search)
     CCF.pcp.project = queryParams.get("project")
     CCF.pcp.pipeline = queryParams.get("pipeline")
  }

  CCF.pcp.updateProjectChanger();
  CCF.pcp.updatePipelineChanger();
  XNAT.xhr.get({
    url: '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/statusSummary?includeSubgroupSummary=false',
    success: function (data) {
        CCF.pcp.doOverviewTableRender(data);
	CCF.pcp.populateNewPipelineIfAny(data);

    },
    fail: function () {
      console.log("Failed to retrieve table data")
    }
  });
}

CCF.pcp.populateRowCount = function() {
  if (typeof CCF.pcp.entity_check !== 'undefined') {
      $('#pcp-row-count-container').html("<em>" + ($(CCF.pcp.entity_check).filter(":visible")).length + " rows match query filters</em>");
  } else {
      $('#pcp-row-count-container').empty();
  }
}

Date.prototype.toISOString = function() {
    var tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function(num) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate()) +
        ' ' + pad(this.getHours()) +
        ':' + pad(this.getMinutes()) +
        ':' + pad(this.getSeconds());
        //dif + pad(tzo / 60) +
        //':' + pad(tzo % 60);
}

CCF.pcp.saveFilterValues = function() {

      CCF.pcp.prevFilter_entityLabel = $(".filter.entityLabel").find(".filter-data").val();
      CCF.pcp.prevFilter_subGroup = $(".filter.subGroup").find(".filter-data").val();
      CCF.pcp.prevFilter_status = $(".filter.status").find(".filter-data").val();
      CCF.pcp.prevFilter_statusTime = $(".filter.statusTime").find(".filter-data").val();
      CCF.pcp.prevFilter_prereqs = $("#pcp-filter-select-prereqs").val();
      CCF.pcp.prevFilter_validated = $("#pcp-filter-select-validated").val();
      CCF.pcp.prevFilter_issues = $("#pcp-filter-select-issues").val();
      CCF.pcp.prevFilter_impeded = $("#pcp-filter-select-impeded").val();
      CCF.pcp.prevFilter_notes = $(".filter.notes").find(".filter-data").val();

}

CCF.pcp.doRenderPipelineTable = function() {

  var tabindex = 0;

      if (CCF.pcp.currentData.length>CCF.pcp.BIG_TABLE_SIZE) {
  	    $("#subgoupFiltersButton").removeClass('hidden');
            if (typeof CCF.pcp.selectedSubgroups === 'undefined') {
		var subgroups = [];
		var nGroups = Math.ceil(CCF.pcp.currentData.length/CCF.pcp.ENTITY_GROUP_SIZE);
                for (var i=0; i<CCF.pcp.currentData.length; i++) {
                    var thisData = CCF.pcp.currentData[i];
                    if (subgroups.indexOf(thisData.subGroup)<0) {
                       subgroups.push(thisData.subGroup);
                    }
                    if (subgroups.length>1 || nGroups>1) {
                       break;
                    }
		}
                if (subgroups.length>1 || nGroups>1) {
                    xmodal.confirm({
                       title: "Subgroup Filter Recommended",
                       height: 270,
                       width: 580,
                       scroll: false,
                       content: "" +
                         "<p><b>WARNING:  This PCP table has over " +  CCF.pcp.BIG_TABLE_SIZE + " rows, which can lead to slow performance.</b></p>" +
                         "<p> You should consider supplying a table-level subgroup filter to reduce the number of rows in the table. Please use the " +
                         " button labeled <em>Subgroup Filters</em> to supply a filter.</p>",
    		   okLabel: 'Make Filter Selection',
    		   cancelLabel: 'Don\'t Apply Filter',
                       okAction: function(){
      			CCF.pcp.subgroupFiltersSelected();
                      },
                       cancelAction: function(){
                            CCF.pcp.selectedSubgroups = [];
                            CCF.pcp.doRenderPipelineTable();
                      },
                    });
               	    return;
    		}
          }
      }

      var displayData;
      if (CCF.pcp.currentData.length<=CCF.pcp.BIG_TABLE_SIZE || typeof CCF.pcp.selectedSubgroups === 'undefined' || CCF.pcp.selectedSubgroups.length==0) {
           displayData = CCF.pcp.currentData;
      } else {
           subgroupData = [];
           var hasEntityGroup = false;
           var hasSubgroupGroup = false;
           //console.log("groupMap",CCF.pcp.groupMap); 
           for (var i=0; i<CCF.pcp.currentData.length; i++) {
               var thisData = CCF.pcp.currentData[i];
               for (var j=0; j<CCF.pcp.selectedSubgroups.length; j++) {
                     var subgroup = CCF.pcp.selectedSubgroups[j];
                     if (!(subgroup.indexOf(":")>=0)) {
                        hasSubgroupGroup = true;
			continue;
                     }
                     hasEntityGroup = true;
                     var subnum = subgroup.replace(/^.*: */g, '');
                     var sublist = CCF.pcp.groupMap.get(parseInt(subnum, 10)); 
                     if (sublist.includes(thisData.entityId)) {
                         subgroupData.push(thisData);
                     }
               }
           }
           if (!hasEntityGroup) {
                subgroupData = CCF.pcp.currentData;
           }
           displayData = [];
           if (hasSubgroupGroup) {
               for (var i=0; i<subgroupData.length; i++) {
                  var thisData = subgroupData[i];
                  if (CCF.pcp.selectedSubgroups.length>0 && CCF.pcp.selectedSubgroups.indexOf(thisData.subGroup)>=0) {
                      displayData.push(thisData);
                  }
              }
          } else {
              displayData = subgroupData;
          }
      }
      XNAT.table.dataTable(displayData, {
        //url: url,
        table: {
          id: 'pcp-' + CCF.pcp.pipeline,
          className: 'pcp-table highlight'
        },
        columns: {
          run: {
            label: '<input id="select-all" name="select-all" type="checkbox">',
            td: {'className': 'center'},
            apply: function() {
    
              CCF.pcp.allStatusEntities.push(this)
              tabindex++
    
              return (
                '<input id="' + this.entityLabel + '::' + this.subGroup + '" ' +
                        'name="entity-check" ' +
                        'class="chkbox" ' +
                        'type="checkbox" ' +
                        'tabindex="'+tabindex+'">'
              )
            }
          },
          entityLabel: {
            label: 'Entity',
            filter: true,
            sort: true,
            td: {'className': 'entityLabel center'},
            apply: function(entity) {
              var html = entity
              if (this.entityType === "xnat:subjectData") {
                href = '/data/projects/'+CCF.pcp.project+'/subjects/'+entity+'?format=html'
                html = '<a href='+href+' target="_blank" >' + entity + '</a>'
              } else if (this.entityType.includes("SessionData") || this.ent) {
                href = '/data/projects/'+CCF.pcp.project+'/experiments/'+entity+'?format=html'
                html = '<a href='+href+' target="_blank" >' + entity + '</a>'
              }
              return html
            }
          },
          subGroup: {
            label: 'Subgroup',
            filter: true,
            sort: true,
            td: {'className': 'subGroup center'},
          },
          status: {
            label: 'Status',
            sort: true,
            filter: true,
            td: {'className': 'status center'},
            apply: function(status) {
              var args = [this.entityLabel, this.subGroup]
              var argsStr = "'" + args.join("','") + "'"
    
              var disabled = ""
              if (!this.statusInfo) {
                disabled = "disabled"
              }
    
              var statusIcons = {
		// Disabling animiation because it causes high CPU usage when there are a lot of running processes
                //'RUNNING': '<i class="fa fa-spinner fa-spin">',
                'RUNNING': '<i class="fa fa-hourglass-half">',
                'COMPLETE': '<i class="fa fa-check">',
                'EXT_COMPLETE': '<i class="fa fa-check">',
                'ERROR': '<i class="fa fa-times">',
                'SUBMITTED': '<i class="fa fa-ellipsis-h">',
                'QUEUED': '<i class="fa fa-ellipsis-h">',
                'NOT_SUBMITTED': '<i class="fa fa-minus">',
                'RESET': '<i class="fa fa-minus">',
                'REMOVED': '<i class="fa fa-minus">',
                'CANCELLED': '<i class="fa fa-minus">',
                'UNKNOWN': '<i class="fa fa-minus">'
              }
              try {
              	var icon = statusIcons[status]
              } catch(err) {
              	var icon = ""
              }
    
              return (
                '<button class="btn btn-sm" style="width:150px" ' + disabled +
                  ' onclick="CCF.pcp.showStatusInfo('+argsStr+')">' +
                   status + " " + icon +
                '</button>'
              )
            }
          },
          statusTime: {
            label: 'StatusTime',
            filter: true,
            sort: true,
            td: {'className': 'statusTime center'},
            apply: function(entity) {
              var dateVar = new Date(this.statusTime);
              var html = dateVar.toISOString();
              //html = "<nobr>" + html.substr(0,html.indexOf(".")) + "</nobr>";
              html = "<nobr>" + html + "</nobr>";
              return html
            }
          },
          prereqs: {
            label: 'Prereqs Met',
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '1' },
                  { label: 'No', value: '0' }
                ],
                element: filterMenuElement.call(table, 'prereqs')
              }).element])
            },
            td: {'className': 'prereqs center'},
            apply: function(prereqs) {
              return CCF.pcp.generateInfoButton(
                this.entityLabel, this.subGroup, prereqs, this.prereqsInfo, 'prereqs')
            }
          },
          validated: {
            label: 'Validated',
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '1' },
                  { label: 'No', value: '0' }
                ],
                element: filterMenuElement.call(table, 'validated')
              }).element])
            },
            td: {'className': 'validated center'},
            apply: function(validated) {
              return CCF.pcp.generateInfoButton(
                this.entityLabel, this.subGroup, validated, this.validatedInfo, 'validated')
            }
          },
          issues: {
            label: 'Issues',
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '0' },
                  { label: 'No', value: '1' }
                ],
                element: filterMenuElement.call(table, 'issues')
              }).element])
            },
            td: {'className': 'issues center'},
            apply: function(issues) {
              return CCF.pcp.generateEditableInfoButton(
                this.entityLabel, this.subGroup, !issues, 'issues')
            }
          },
          impeded: {
            label: "Runnable",
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '1' },
                  { label: 'No', value: '0' }
                ],
                element: filterMenuElement.call(table, 'impeded')
              }).element])
            },
            td: {'className': 'impeded center'},
            apply: function(impeded) {
              return CCF.pcp.generateEditableInfoButton(
                this.entityLabel, this.subGroup, !impeded, 'impeded')
            }
          },
          notes: {
            label: 'Notes',
            filter: true,
            td: {'className': 'notes center'},
            apply: function(notes) {
              var icon = '<i class="fa fa-comment-o" aria-hidden="true"></i>'
              if (notes) {
                icon = '<i class="fa fa-commenting-o" aria-hidden="true"></i>'
              }
    
              var args = [this.entityLabel, this.subGroup]
              var argsStr = "'" + args.join("','") + "'"
    
              return (
                '<button class="btn btn-sm" ' +
                  'onclick="CCF.pcp.showNotes('+argsStr+')">' + icon +
                '</button>'
               )
            }
          }
        }
      }).render($('#pcp-table-container'))
      CCF.pcp.entity_check = $('input[name=entity-check]');
      CCF.pcp.populateRowCount();
      $('input.filter-data').keyup(function() {
          setTimeout(function(){
             CCF.pcp.populateRowCount();
         },50)
      });
      initSelectAll();
      initShiftSelect();
      if (typeof CCF.pcp.prevFilter_entityLabel !== 'undefined' && CCF.pcp.prevFilter_entityLabel.length>0) {
          $(".filter.entityLabel").find(".filter-data").val(CCF.pcp.prevFilter_entityLabel).focus().keyup();
      }
      if (typeof CCF.pcp.prevFilter_subGroup !== 'undefined' && CCF.pcp.prevFilter_subGroup.length>0) {
          $(".filter.subGroup").find(".filter-data").val(CCF.pcp.prevFilter_subGroup).focus().keyup();
      }
      if (typeof CCF.pcp.prevFilter_status !== 'undefined' && CCF.pcp.prevFilter_status.length>0) {
          $(".filter.status").find(".filter-data").val(CCF.pcp.prevFilter_status).focus().keyup();
      }
      if (typeof CCF.pcp.prevFilter_statusTime !== 'undefined' && CCF.pcp.prevFilter_statusTime.length>0) {
          $(".filter.statusTime").find(".filter-data").val(CCF.pcp.prevFilter_statusTime).focus().keyup();
      }
      if (typeof CCF.pcp.prevFilter_prereqs !== 'undefined' && CCF.pcp.prevFilter_prereqs.length>0 && CCF.pcp.prevFilter_prereqs !== 'all') {
          $("#pcp-filter-select-prereqs").val(CCF.pcp.prevFilter_prereqs).focus().keyup().change();
      }
      if (typeof CCF.pcp.prevFilter_validated !== 'undefined' && CCF.pcp.prevFilter_validated.length>0 && CCF.pcp.prevFilter_validated !== 'all') {
          $("#pcp-filter-select-validated").val(CCF.pcp.prevFilter_validated).focus().keyup().change();
      }
      if (typeof CCF.pcp.prevFilter_issues !== 'undefined' && CCF.pcp.prevFilter_issues.length>0 && CCF.pcp.prevFilter_issues !== 'all') {
          $("#pcp-filter-select-issues").val(CCF.pcp.prevFilter_issues).focus().keyup().change();
      }
      if (typeof CCF.pcp.prevFilter_impeded !== 'undefined' && CCF.pcp.prevFilter_impeded.length>0 && CCF.pcp.prevFilter_impeded !== 'all') {
          $("#pcp-filter-select-impeded").val(CCF.pcp.prevFilter_impeded).focus().keyup().change();
      }
      if (typeof CCF.pcp.prevFilter_notes !== 'undefined' && CCF.pcp.prevFilter_notes.length>0) {
          $(".filter.notes").find(".filter-data").val(CCF.pcp.prevFilter_notes).focus().keyup();
      }
      $("tr.filter").find("td").not(".filter").html("<button style='color:black;background-color:white;padding:3px;' onclick='CCF.pcp.clearFilters()'>Clr</button>");

}

CCF.pcp.clearFilters = function() {
     CCF.pcp.prevFilter_entityLabel = "";
     CCF.pcp.prevFilter_subGroup = "";
     CCF.pcp.prevFilter_status = ""
     CCF.pcp.prevFilter_statusTime = ""
     CCF.pcp.prevFilter_prereqs = "all";
     CCF.pcp.prevFilter_validated = "all";
     CCF.pcp.prevFilter_issues = "all";
     CCF.pcp.prevFilter_impeded = "all";
     CCF.pcp.prevFilter_notes = "";
     $(".filter.entityLabel").find(".filter-data").val(CCF.pcp.prevFilter_entityLabel);
     $(".filter.subGroup").find(".filter-data").val(CCF.pcp.prevFilter_subGroup);
     $(".filter.status").find(".filter-data").val(CCF.pcp.prevFilter_status);
     $(".filter.statusTime").find(".filter-data").val(CCF.pcp.prevFilter_statusTime);
     $("#pcp-filter-select-prereqs").val(CCF.pcp.prevFilter_prereqs);
     $("#pcp-filter-select-validated").val(CCF.pcp.prevFilter_validated);
     $("#pcp-filter-select-issues").val(CCF.pcp.prevFilter_issues);
     $("#pcp-filter-select-impeded").val(CCF.pcp.prevFilter_impeded);
     $(".filter.notes").find(".filter-data").val(CCF.pcp.prevFilter_notes);
     setTimeout(function() {
         $(".filter.entityLabel").find(".filter-data").focus().keyup();
         $(".filter.subGroup").find(".filter-data").focus().keyup();
         $(".filter.status").find(".filter-data").focus().keyup();
         $(".filter.statusTime").find(".filter-data").focus().keyup();
         $("#pcp-filter-select-prereqs").focus().keyup().change();
         $("#pcp-filter-select-validated").focus().keyup().change();
         $("#pcp-filter-select-issues").focus().keyup().change();
         $("#pcp-filter-select-impeded").focus().keyup().change();
         $(".filter.notes").find(".filter-data").focus().keyup();
     }, 50);
}

CCF.pcp.renderPipelineTable = function(cached=true) {

  CCF.pcp.saveFilterValues();
  $('#pcp-table-container').empty()
  $('#pcp-row-count-container').empty();
  CCF.pcp.allStatusEntities = []

  var loadingMsg = "Loading cached table"
  if (!cached) {
    loadingMsg = "Updating control panel cache..."
    try {
      xmodal.loading.open(loadingMsg)
    } catch(err) {
      console.log("xmodal.loading.open failed")
    }
  }
  // xmodal doesn't seem to be loaded at this point for whatever reason
  // dialog.loading works, but can't close since there's no dataTable.done() method
  // xmodal.loading.open(loadingMsg)
  // XNAT.ui.dialog.loading.open()

  var url = serverRoot + '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
     '/pipeline/' + CCF.pcp.pipeline +
     '/status?cached=' + cached.toString() + '&condensed=false';

  var status_update_url = serverRoot + '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
     '/pipeline/' + CCF.pcp.pipeline +
     '/status?cached=' + cached.toString() + '&condensed=false';

  CCF.pcp.updateProjectChanger(CCF.pcp.pipeline);
  CCF.pcp.updatePipelineChanger(CCF.pcp.pipeline);
  XNAT.xhr.get({
  	url: url,
	cache: false,
	async: true,
	context: this,
  }).done( function(data, textStatus, jqXHR) {


      if (jqXHR.status == 202) {
          msg = "<b>The update process has returned early because it is expected to take quite some time to run. " +
                " Only some of the rows have completed processing.</b><br><br>The process will keep running and " +
		"you may refresh or check this page again " +
                " later to see the final results.";
          xmodal.message({ title: "Incomplete",
			width: '500px',
			height: '300px',
			content: msg
		});
      }
      if (data.length<1) {
	  	$('#pcp-table-container').html("<div style='margin-left:10px;margin-right:80px;margin-top:10px;max-width:1000px;'><h2>No pipeline control panel data for this pipeline</h2>" +
			"This may be because this pipeline was recently configured and the status update job has not yet run.  You may click " +
  			'<button class="btn" onclick="CCF.pcp.renderPipelineTable(false)">HERE</button> to request a status update run and refresh of this page, ' +
			"however in many cases a full status update run takes quite some time to run." +  
			"</div>");
		return;
      }

      CCF.pcp.currentData = data;
      CCF.pcp.currentData.sort(function(a,b) {
		var retVal = a.entityLabel.localeCompare(b.entityLabel);
		if (retVal == 0) {
			retVal = a.subGroup.localeCompare(b.subGroup);
		}
		return retVal;
      });
      CCF.pcp.doRenderPipelineTable();

  }).fail( function(data, textStatus, jqXHR) {
		xmodal.message("Error","ERROR:  Could not build the pipeline control panel table.");
  });

}

function initSelectAll() {
  $('#select-all').click(function(event) {
    if(this.checked) {
      $(':checkbox').each(function() {
        // Make sure it's not a hidden/filtered row
        var $row = $(this).closest('tr')
        // console.log($row.css("display"))
        if ($row.css("display") === "table-row") {
          this.checked = true;
        }
      });
    } else {
      $(':checkbox').each(function() {
        this.checked = false;
      });
    }
  });

  // Disable this here for now
  // Eventually goes away once custom filter implemented
  $("input[title='notes:filter']").attr('disabled', 'disabled')
  $("input[title='notes:filter']").attr('placeholder', 'Not implemented')
}

function initShiftSelect() {
  var lastChecked = null;
  var $chkboxes = $('.chkbox')

  $chkboxes.click(function(e) {
      if(!lastChecked) {
          lastChecked = this
          return
      }
      $chkboxes = $('.chkbox')
      if(e.shiftKey) {
        var startChecking = false;
        var hasChecked = false;
        $chkboxes.each(function(inx,val) {
           if (val==lastChecked) {
              startChecking = !startChecking;
              if (hasChecked && !startChecking) {
                 return false;
              }
           } else if (val==e.srcElement) {
              startChecking = !startChecking;
              if (hasChecked && !startChecking) {
                 return false;
              }
           } else if (startChecking && $(val).is(":visible")) {
              hasChecked = true;
              var tr = $(val).closest('tr');
              if (typeof tr === 'undefined') {
                 return false;
              } 
              trClass = tr.attr('class');
              $(val).prop('checked', lastChecked.checked)
           }
        })
      }
      lastChecked = this
  });
}

// set up custom filter menus
function filterMenuElement(prop){
  if (!prop) return false;
  // call this function in context of the table
  var $pipelineTable = $(this);
  var FILTERCLASS = 'filter-' + prop;
  return {
    id: 'pcp-filter-select-' + prop,
    on: {
      change: function() {
        var selectedValue = $(this).val();
        // console.log(selectedValue);

        $pipelineTable.find('button[class*="filter-'+prop+'"]').each(function() {
          var $row = $(this).closest('tr');
          // console.log($row)
          if (selectedValue === 'all') {
            $row.removeClass(FILTERCLASS);
            return;
          }

          $row.addClass(FILTERCLASS);
          // if (selectedValue == this.textContent) {
          if (this.textContent.includes(selectedValue)) {
            $row.removeClass(FILTERCLASS);
          }
        })

	CCF.pcp.populateRowCount();

      }
    }
  };
}

CCF.pcp.generateInfoButton = function(entity, group, successful, infoText, key) {
  var icon = '<i class="fa fa-times" style="color:red"></i>'
  var filterVal = 0
  if (successful) {
    icon = '<i class="fa fa-check" style="color:green"></i>'
    filterVal = 1
  }

  var args = [entity, group, key]
  var argsStr = "'" + args.join("','") + "'"
  // var disabled = "disabled" ? infoText: ""
  var disabled = ""
  if (!infoText) {
    disabled = "disabled"
  }

  return (
    '<button class="btn btn-sm filter-'+key+'" ' + disabled +
      ' onclick="CCF.pcp.showEntityInfo('+argsStr+')">' + icon + ' ' +
      ' <i class="hidden sorting filtering '+key+'">'+filterVal+'</i>' +
    '</button>'
  )
}

CCF.pcp.generateEditableInfoButton = function(entity, group, successful, key) {
  var icon = '<i class="fa fa-times" style="color:red"></i>'
  var filterVal = 0
  if (successful) {
    icon = '<i class="fa fa-check" style="color:green"></i>'
    filterVal = 1
  }

  var args = [entity, group, key]
  var argsStr = "'" + args.join("','") + "'"

  return (
    '<button class="btn btn-sm filter-'+key+'"' +
      ' onclick="CCF.pcp.showEditableInfo('+argsStr+')">' + icon + ' ' +
      ' <i class="hidden sorting filtering '+key+'">'+filterVal+'</i>' +
    '</button>'
  )
}

CCF.pcp.showStatusInfo = function(entity, group) {

  CCF.pcp.getStatusEntity(entity, group)
  var modalTitle = CCF.pcp.selectedEntity["entityLabel"] + " status"
  var submittedDateStr = ""
  var updatedDateStr = ""

  if (CCF.pcp.selectedEntity.statusTime) {
    updatedDateStr = updatedDateStr += new Date(CCF.pcp.selectedEntity.statusTime)
  }
  if (CCF.pcp.selectedEntity.submitTime) {
    submittedDateStr = "<b>Submitted on:&nbsp; </b> "
    submittedDateStr += new Date(CCF.pcp.selectedEntity.submitTime)
    submittedDateStr += "<br><b>Submit node:&nbsp; </b> "
    submittedDateStr += CCF.pcp.selectedEntity.submitNode
  }

  var content = '<div>' + CCF.pcp.selectedEntity.statusInfo + '</div>' +
    '<div>' + submittedDateStr + '</div>'
    // '<div><p>' + updatedDateStr + '</div>'

  XNAT.ui.dialog.open({
    title: modalTitle,
    // width: '600px',
    // height: '400px',
    content: content,
    footerContent: updatedDateStr,
    buttons: [{
      label: 'OK',
      default: true,
      close: true
    }]
  });
}

CCF.pcp.showEntityInfo = function(entity, group, key) {

  CCF.pcp.getStatusEntity(entity, group)
  var modalTitle = CCF.pcp.selectedEntity["entityLabel"] + " " + key + " info"
  var dateStr = ""
  if (CCF.pcp.selectedEntity[key + 'Time']) {
    dateStr = new Date(CCF.pcp.selectedEntity[key + 'Time'])
  }

  // var content = '<div>' + CCF.pcp.selectedEntity[key + 'Info'] + '</div><hr>' +
  //   '<div>' + dateStr + '</div>'
  var content = '<div>' + CCF.pcp.selectedEntity[key + 'Info'] + '</div>'

  XNAT.ui.dialog.open({
    title: modalTitle,
    content: content,
    width: '650px',
    footer: dateStr,
    buttons: [{
      label: 'OK',
      default: true
    }]
  });
}

CCF.pcp.showEditableInfo = function(entity, group, key) {

  CCF.pcp.getStatusEntity(entity, group)

  var modalTitle = CCF.pcp.selectedEntity["entityLabel"] + " " + key + " info"
  var dateStr = ""
  if (CCF.pcp.selectedEntity[key + 'Time']) {
    dateStr = new Date(CCF.pcp.selectedEntity[key + 'Time'])
  }

  XNAT.ui.dialog.open({
    title: modalTitle,
    esc: true,
    content: '<div id="pcp-editable-info"></div>',
    footerContent: dateStr,
    beforeShow: function(obj) {
      var container = obj.$modal.find('#pcp-editable-info')
      renderInfoPanel(container)
    },
    buttons: [
    {
      label: "Save",
      default: true,
      action: function() {
        if ($('#'+key).val() === "false") {
          CCF.pcp.selectedEntity[key] = false
        } else {
          CCF.pcp.selectedEntity[key] = true
        }
        CCF.pcp.selectedEntity[key+'Info'] = $('#'+key+'Info').val()
        // CCF.pcp.selectedEntity[key + 'Time'] = Number(new Date())
        CCF.pcp.updateStatusEntity()
        CCF.pcp.renderPipelineTable()
      }
    },
    {
      label: 'Cancel',
      default: false,
      close: true
    }]
  });

  function renderInfoPanel(container) {
    var dropdown =
    '<div class="panel-element" style="display:inline-block">' +
      '<label class="element-label" style="display:inline-block">' + key + '?</label>' +
      '<div class="element-wrapper">' +
        '<select id="'+key+'" title="'+key+'">' +
          '<option value="true">Yes</option>' +
          '<option value="false">No</option>' +
        '</select>' +
      '</div>' +
    '</div>'
    $(container).append(dropdown)
    $('#'+key).val(CCF.pcp.selectedEntity[key].toString())

    // var $textArea = $('<br><br><textarea id="'+key+'Info" rows=8 cols=75/>')
    var $textArea = $('<br><br><textarea id="'+key+'Info" rows=12 style="min-height:120px; height:100%; width:100%"/>')
    $textArea.text(CCF.pcp.selectedEntity[key+'Info']);
    $(container).append($textArea)
  }
}

CCF.pcp.showNotes = function(entity, group) {
  CCF.pcp.getStatusEntity(entity, group)

  var tempDiv = spawn('div', CCF.pcp.selectedEntity.notes);

  var editorConfig = {
      language: 'text'
  }

  var editor = XNAT.app.codeEditor.init(tempDiv, editorConfig);

  editor.openEditor({
      title: 'Notes for ' + CCF.pcp.selectedEntity.entityLabel + ' ' + CCF.pcp.selectedEntity.subGroup,
      id: "pcp-notes-modal",
      esc: true,
      buttons: {
        save: {
          label: 'Save',
          isDefault: true,
          action: function(modal) {
            CCF.pcp.selectedEntity.notes = editor.getValue().code;
            CCF.pcp.updateStatusEntity()
            CCF.pcp.renderPipelineTable()
            modal.close()
          }
        },
        cancel: {
          label: 'Cancel',
          action: function(modal){
            modal.close()
          }
        }
      }
      // other xmodal properties
  });
}

CCF.pcp.renderStatusUpdate = function() {

  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities.length < 1) {
     XNAT.ui.banner.top(3000, "Please select at least one row to update.")
    return
  } else {

     xmodal.confirm({
        height: 220,
        scroll: false,
        content: "" +
          "<p>Run status update for " + CCF.pcp.submissionEntities.length + " rows?</p>",
        okAction: function(){
          var submitUrl = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/pipeline/' + CCF.pcp.pipeline + '/updateStatusEntities'
          //console.log(JSON.stringify(submitJson))

          XNAT.ui.banner.top(4000, "Processing status update.  The table will reload when processing completes. ");
          $.ajax({
            type: "POST",
            url: submitUrl,
            cache: false,
            async: true,
            contentType: "application/json; charset=utf-8",
            context: this,
            data: JSON.stringify(CCF.pcp.submissionEntities),
            dataType: 'json'
          }).done( function(data, textStatus, jqXHR) {
            var msg;
            if (jqXHR.status == 202) {
               msg = "<b>The update process has returned early because it is expected to take approximately " + jqXHR.responseText + " seconds to run. " +
                     " Only some of the selected rows have been updated.</b><br><br>The process will keep running and you may refresh or check this page again " +
                     " later to see the final results.";
               xmodal.message({ title: "Incomplete",
				width: '500px',
				height: '300px',
				content: msg
			});
	    } else {
               msg = "Status has been updated for all selected rows";
               XNAT.ui.banner.top(3000, msg);
            }
            CCF.pcp.renderPipelineTable(true);
          }).fail( function(data, textStatus, jqXHR) {
            $(".top-banner").hide();
            xmodal.message("Processing Error","ERROR: Could not process the update.  This is likely because an update process is currently running for this project.");
            CCF.pcp.renderPipelineTable(true)
          });
	},
        cancelAction: function(){
          XNAT.ui.banner.top(3000, "Update operation cancelled.");
          CCF.pcp.renderPipelineTable(true);
	},
     });

  }

}

CCF.pcp.resetStatusSelected = function() {

  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities.length < 1) {
     XNAT.ui.banner.top(3000, "Please select at least one row to reset.")
    return
  } else {

     xmodal.confirm({
        height: 220,
        scroll: false,
        content: "" +
          "<p>Reset status for " + CCF.pcp.submissionEntities.length + " rows?</p>",
        okAction: function(){
          var submitUrl = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/pipeline/' + CCF.pcp.pipeline + '/setValues?status=RESET'
          //console.log(JSON.stringify(submitJson))

          XNAT.ui.banner.top(4000, "Processing reset.  The table will reload when processing completes. ");
          $.ajax({
            type: "POST",
            url: submitUrl,
            cache: false,
            async: true,
            contentType: "application/json; charset=utf-8",
            context: this,
            data: JSON.stringify(CCF.pcp.submissionEntities),
            dataType: 'json'
          }).done( function(data, textStatus, jqXHR) {
            var msg = "Status has been reset for all selected rows";
            XNAT.ui.banner.top(3000, msg);
            CCF.pcp.renderPipelineTable(true);
          }).fail( function(data, textStatus, jqXHR) {
            $(".top-banner").hide();
            xmodal.message("Processing Error","ERROR: Could not process the reset operation.  This is likely because an update process is currently running for this project.");
            CCF.pcp.renderPipelineTable(true)
          });
	},
        cancelAction: function(){
          XNAT.ui.banner.top(3000, "StatusReset operation cancelled.");
          CCF.pcp.renderPipelineTable(true);
	},
     });

  }

}


CCF.pcp.subgroupFiltersSelected = function() {

  CCF.pcp.saveFilterValues();
  $('#pcp-table-container').empty()
  $('#pcp-row-count-container').empty();

  if (typeof CCF.pcp.currentSubgroups === 'undefined') {
	CCF.pcp.currentSubgroups = [];
        for (var i=0; i<CCF.pcp.currentData.length; i++) {
            var thisData = CCF.pcp.currentData[i];
            var groupSubgroup = CCF.pcp.updateGroupMap(thisData,i,CCF.pcp.currentData.length);
            if (CCF.pcp.currentSubgroups.indexOf(groupSubgroup)<0) {
               CCF.pcp.currentSubgroups.push(groupSubgroup);
            }
            if (CCF.pcp.currentSubgroups.indexOf(thisData.subGroup)<0) {
               CCF.pcp.currentSubgroups.push(thisData.subGroup);
            }
	}
	CCF.pcp.currentSubgroups.sort(function(a,b) {
			var inx_a = a.indexOf(":");
			inx_a = (inx_a>=0) ? inx_a : 999;
			var inx_b = b.indexOf(":");
			inx_b = (inx_b>=0) ? inx_b : 999;
			if (inx_a != inx_b) {
				return inx_a - inx_b;
			} else {
				return a.localeCompare(b);
			}
	});
   }

   var selectSize = (CCF.pcp.currentSubgroups.length<=15) ? CCF.pcp.currentSubgroups.length : 15;
   var contentStr = 
             "<h3><b>Select subgroups</b></h3><br>" +
             "<select style='min-width:320px' size=\"" + selectSize + "\" multiple id='_subgroupFilter'>";
   for (var i=0; i<CCF.pcp.currentSubgroups.length; i++) {
        var thisSubgroup = CCF.pcp.currentSubgroups[i];
        contentStr = contentStr + "<option value=\"" + thisSubgroup + "\"";
        if (typeof CCF.pcp.selectedSubgroups !== 'undefined') {
            if (CCF.pcp.selectedSubgroups.indexOf(thisSubgroup)>=0) {
                contentStr = contentStr + " selected=\'selected\'";
            }
        }    
        contentStr = contentStr + ">" + thisSubgroup + "</option>";
   }
        contentStr = contentStr + "</select>";
		
    xmodal.confirm({
       height: 400,
       width: 500,
       scroll: false,
       content: contentStr,
	   okLabel: 'OK',
	   cancelLabel: 'Clear Subgroup Filters',
           okAction: function(){
        	CCF.pcp.selectedSubgroups = $("#_subgroupFilter").val();
                CCF.pcp.doRenderPipelineTable();
           },
           cancelAction: function(){
        	CCF.pcp.selectedSubgroups = [];
                CCF.pcp.doRenderPipelineTable();
           }
    });

}


CCF.pcp.infoReportSelected = function() {

  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities.length < 1) {
     XNAT.ui.banner.top(3000, "Please select at least one row for the report.")
    return
  } else {

	xmodal.confirm({
		height: 420,
		width: 360,
		scroll: false,
		content: 
			"<p><h3>Generate report for " + CCF.pcp.submissionEntities.length + " rows?</h3></p>" +
			"<p>Please select desired info fields: <br/><select style='margin-left:50px;margin-top:20px;height:100px' multiple id='report-select'>" + 
			"<option value='status'>Status</option>" +
			"<option value='prereqs'>PrereqsMet</option>" +
			"<option value='validated'>Validated</option>" +
			"<option value='issues'>Issues</option>" +
			"<option value='runnable'>Runnable</option>" +
			"<option value='notes'>Notes</option>" +
			"</select></p>",
		okAction: function(modl){
			var reportSelect = $('#report-select').val();
			if (reportSelect == null || reportSelect == "null") {
				xmodal.message("Error","You must select one or more fields for the report.");
				return;
			}
			var reportFields = reportSelect.toString().split(",");
			xmodal.open({
				height: 620,
				width: 930,
				title: "PCP InfoField Report",
				scroll: true,
				overflow: 'auto',
				maximize: true,
				//content: "<div id='pcp-infofield-report' style='font-size:12px'></div>",
				content: "<div id='pcp-infofield-report'></div>",
				okLabel: 'Done',
				//okAction: function(){
				//},
				cancel: 'show',
				cancelLabel: 'Print Report',
				cancelAction: function(){
					CCF.pcp.doPrint($("#pcp-infofield-report").html());
				},
			});
			$("#pcp-infofield-report").html(CCF.pcp.generateReport(reportFields));
			//$("#pcp-infofield-report").find('p').css('font-size','12px');
			modl.close();
		},
		okClose: false,
   		cancelAction: function(){
			XNAT.ui.banner.top(3000, "Info report request cancelled.");
			CCF.pcp.renderPipelineTable(true);
		}
	});

  }

}

CCF.pcp.generateReport = function(reportFields) {
	var reportHtml = "<h2 style='width:100%;text-align:center'>" + CCF.pcp.pipeline + " InfoField Report</h2>";
    	for (var i = 0; i < CCF.pcp.submissionEntities.length; i++) {
		var entity = CCF.pcp.submissionEntities[i];
		reportHtml = reportHtml + "<p><h2>Entity:  " + entity.entityLabel + "</h2></p>";
		for (var j = 0; j < reportFields.length; j++) {
			var field = reportFields[j];
			if (field == 'status') {
				if (reportFields.length>1) {
					reportHtml = reportHtml + "<p>StatusInfo:</p>";
				}
				var statusInfo = entity.statusInfo;
				if (statusInfo.indexOf("RULESERROR:")>0 && statusInfo.indexOf("EXCEPTION TRACE:")>0) {
					statusInfo = statusInfo.substr(0,statusInfo.indexOf("EXCEPTION TRACE:"));
				}
				reportHtml = reportHtml + "<p>" + statusInfo + "</p>";
			} else if (field == 'prereqs') {
				if (reportFields.length>1) {
					reportHtml = reportHtml + "<p>PrereqsInfo:</p>";
				}
				reportHtml = reportHtml + "<p>" + entity.prereqsInfo + "</p>";
			} else if (field == 'validated') {
				if (reportFields.length>1) {
					reportHtml = reportHtml + "<p>ValidatedInfo:</p>";
				}
				reportHtml = reportHtml + "<p>" + entity.validatedInfo + "</p>";
			} else if (field == 'issues') {
				if (reportFields.length>1) {
					reportHtml = reportHtml + "<p>IssuesInfo:</p>";
				}
				reportHtml = reportHtml + "<p>" + entity.issuesInfo + "</p>";
			} else if (field == 'runnable') {
				if (reportFields.length>1) {
					reportHtml = reportHtml + "<p>RunnableInfo:</p>";
				}
				reportHtml = reportHtml + "<p>" + entity.impededInfo + "</p>";
			} else if (field == 'notes') {
				if (reportFields.length>1) {
					reportHtml = reportHtml + "<p>Notes:</p>";
				}
				reportHtml = reportHtml + "<p>" + entity.notes + "</p>";
			}
		}
    	}
	return reportHtml;
}

CCF.pcp.csvDownloadSelected = function() {

  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities.length < 1) {
     XNAT.ui.banner.top(3000, "Please select at least one row for the report.")
    return
  } else {

	xmodal.confirm({
                title: "Download Summary CSV",
		height: 240,
		width: 360,
		scroll: false,
		content: "<p><h3>Exclude information fields?</h3></p><p><input type='checkbox' id='compress-html-checkbox' checked> Compress HTML tags from output</p>",
		okLabel: 'Yes',
		//okAction: function(){
		//},
		cancel: 'show',
		cancelLabel: 'No',
		okAction: function(modl){
			CCF.pcp.downloadSummaryCSV(false,$('#compress-html-checkbox').is(':checked'));
			modl.close();
		},
		okClose: true,
   		cancelAction: function(modl){
			CCF.pcp.downloadSummaryCSV(true,$('#compress-html-checkbox').is(':checked'));
			modl.close();
		}
	});

  }

}

CCF.pcp.downloadSummaryCSV = function(includeInfoFields,compressHtmlTags) {
	//var csvContent = "data:text/csv;charset=utf-8,";
	var csvContent = "";
	csvContent += "\"project\",";
	csvContent += "\"pipeline\",";
	csvContent += "\"entityLabel\",";
	csvContent += "\"entityId\",";
	csvContent += "\"entityType\",";
	csvContent += "\"subGroup\",";
	csvContent += "\"status\",";
	csvContent += "\"statusTime\",";
	if (includeInfoFields) {
		csvContent += "\"statusInfo\",";
	}
	csvContent += "\"prereqs\",";
	if (includeInfoFields) {
		csvContent += "\"prereqsInfo\",";
	}
	csvContent += "\"validated\",";
	if (includeInfoFields) {
		csvContent += "\"validatedInfo\",";
	}
	csvContent += "\"issues\",";
	if (includeInfoFields) {
		csvContent += "\"issuesInfo\",";
	}
	if (includeInfoFields) {
		csvContent += "\"impeded\",";
		csvContent += "\"impededInfo\",";
		csvContent += "\"notes\"";
	} else {
		csvContent += "\"impeded\"";
	}
	csvContent += '\r\n';
	var entity;
    	for (var i = 0; i < CCF.pcp.submissionEntities.length; i++) {
		entity = CCF.pcp.submissionEntities[i];
		//csvContent = csvContent + "<p><h2>Entity:  " + entity.entityLabel + "</h2></p>";
		csvContent += "\"" + entity.project.replace(/"/g, '\\"') + "\",";
		csvContent += "\"" + entity.pipeline.replace(/"/g, '\\"') + "\",";
		csvContent += "\"" + entity.entityLabel.replace(/"/g, '\\"') + "\",";
		csvContent += "\"" + entity.entityId.replace(/"/g, '\\"') + "\",";
		csvContent += "\"" + entity.entityType.replace(/"/g, '\\"') + "\",";
		csvContent += "\"" + entity.subGroup.replace(/"/g, '\\"') + "\",";
		csvContent += "\"" + entity.status.replace(/"/g, '\\"') + "\",";
		var dateVar = new Date(entity.statusTime);
		var dateStr = dateVar.toISOString();
		csvContent += "\"" + dateStr + "\",";
		if (includeInfoFields) {
			if (compressHtmlTags) {
				csvContent += "\"" + entity.statusInfo.replace(/"/g, '\\"').replace(/(<([^>]+)>)/ig,"") + "\",";
			} else {
				csvContent += "\"" + entity.statusInfo.replace(/"/g, '\\"') + "\",";
			}
		}
		csvContent += "\"" + entity.prereqs + "\",";
		if (includeInfoFields) {
			if (compressHtmlTags) {
				csvContent += "\"" + entity.prereqsInfo.replace(/"/g, '\\"').replace(/(<([^>]+)>)/ig,"") + "\",";
			} else {
				csvContent += "\"" + entity.prereqsInfo.replace(/"/g, '\\"') + "\",";
			}
		}
		csvContent += "\"" + entity.validated + "\",";
		if (includeInfoFields) {
			if (compressHtmlTags) {
				csvContent += "\"" + entity.validatedInfo.replace(/"/g, '\\"').replace(/(<([^>]+)>)/ig,"") + "\",";
			} else {
				csvContent += "\"" + entity.validatedInfo.replace(/"/g, '\\"') + "\",";
			}
		}
		csvContent += "\"" + entity.issues + "\",";
		if (includeInfoFields) {
			if (compressHtmlTags) {
				csvContent += "\"" + entity.issuesInfo.replace(/"/g, '\\"').replace(/(<([^>]+)>)/ig,"") + "\",";
			} else {
				csvContent += "\"" + entity.issuesInfo.replace(/"/g, '\\"') + "\",";
			}
		}
		if (includeInfoFields) {
			csvContent += "\"" + entity.impeded + "\",";
			if (compressHtmlTags) {
				csvContent += "\"" + entity.impededInfo.replace(/"/g, '\\"').replace(/(<([^>]+)>)/ig,"") + "\",";
				csvContent += "\"" + entity.notes.replace(/"/g, '\\"').replace(/(<([^>]+)>)/ig,"") + "\"";
			} else {
				csvContent += "\"" + entity.impededInfo.replace(/"/g, '\\"') + "\",";
				csvContent += "\"" + entity.notes.replace(/"/g, '\\"') + "\"";
			}
		} else {
			csvContent += "\"" + entity.impeded + "\"";
		}
		csvContent += '\r\n';
    	}
	var a = document.createElement("a");
	document.body.appendChild(a);
	a.style = "display: none";
	blob = new Blob([csvContent], {type: "text/csv"}),
	url = window.URL.createObjectURL(blob);
	a.href = url;
	a.download = "pcp_summary_" + entity.project + "_" + entity.pipeline + ".csv";
	a.click();
	window.URL.revokeObjectURL(url);
}



CCF.pcp.doPrint = function(printStr) {

  $('<iframe>', {
    name: 'myiframe',
    class: 'printFrame'
  })
  .appendTo('body')
  .contents().find('body')
  .append(printStr);

  window.frames['myiframe'].focus();
  window.frames['myiframe'].print();

  setTimeout(() => { $(".printFrame").remove(); }, 1000);

}


CCF.pcp.removeSelected = function() {

  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities.length < 1) {
     XNAT.ui.banner.top(3000, "Please select at least one row to update.")
    return
  } else {

     xmodal.confirm({
        height: 220,
        scroll: false,
        content: "" +
          "<p style='color:#AA0000;font-weight:bold'>Are you sure you want to remove " + CCF.pcp.submissionEntities.length + " rows?</p>",
        okAction: function(){
          var submitUrl = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/pipeline/' + CCF.pcp.pipeline + '/deleteStatusEntities'
          //console.log(JSON.stringify(submitJson))

          XNAT.ui.banner.top(4000, "Processing status update.  The table will reload when processing completes. ");
          $.ajax({
            type: "DELETE",
            url: submitUrl,
            cache: false,
            async: true,
            contentType: "application/json; charset=utf-8",
            context: this,
            data: JSON.stringify(CCF.pcp.submissionEntities),
            dataType: 'json'
          }).done( function(data, textStatus, jqXHR) {
            var msg;
            if (jqXHR.status == 202) {
               msg = "<b>The update process has returned early because it is expected to take approximately " + jqXHR.responseText + " seconds to run. " +
                     " Only some of the selected rows have been updated.</b><br><br>The process will keep running and you may refresh or check this page again " +
                     " later to see the final results.";
               xmodal.message({ title: "Incomplete",
				width: '500px',
				height: '300px',
				content: msg
			});
	    } else {
               msg = "Row removal completed."; 
               XNAT.ui.banner.top(3000, msg);
            }
            CCF.pcp.renderPipelineTable(true);
          }).fail( function(data, textStatus, jqXHR) {
            $(".top-banner").hide();
            xmodal.message("Processing Error","ERROR: Could not process the delete process for all records.  This may be because you don't have permission to perform this operation.");
            CCF.pcp.renderPipelineTable(true)
          });
	},
        cancelAction: function(){
          XNAT.ui.banner.top(3000, "Remove operation cancelled.");
          CCF.pcp.renderPipelineTable(true);
	},
     });

  }

}

CCF.pcp.selectPipelineToLaunch = function() {
  // Make sure at least one entity is selected before rendering modal
  CCF.pcp.getSubmissionEntities()
  if (typeof CCF.pcp.submissionEntities == 'undefined' || CCF.pcp.submissionEntities < 1) {
    XNAT.ui.banner.top(3000, "Please select at least one entity before launching")
    return
  }
  if (typeof CCF.pcp.pipelineConfig == 'undefined' || CCF.pcp.pipelineConfig.length<=1) {
    CCF.pcp.submitPipeline = this.pipeline;
    CCF.pcp.renderSubmitParameters();
    return;
  }
  XNAT.ui.dialog.open({
    title: "Select Pipeline",
    content:  '<div class="panel">' +
                '<div id="pcp-pipelines"><b>Select pipeline to launch:</b> <select id="pipelineSelector"></select>' + 
                '<br><br>NOTE:  If you select a different pipeline than the default (current page) pipeline, only entities ' +
                'with matching project/entity/subgroup values for that pipeline will be run.' +
                '</div><hr>' +
              '</div>',
    beforeShow: function(obj) {
       var pConfig = CCF.pcp.pipelineConfig;
       var $selct = $("#pipelineSelector");
       for (var i=0; i<pConfig.length; i++) {
           var pipe = pConfig[i].pipeline;
           if (typeof pipe !== 'undefined') {
              $selct.append("<option value='" + pipe + "'>" + pipe + "</option>");
              if (pipe == CCF.pcp.pipeline) {
                  $selct.val(pipe);
              }
           }
       }
    },
    buttons: [
    {
        label: 'Continue',
        isDefault: true,
        close: true,
        action: function() {
            CCF.pcp.submitPipeline = $("#pipelineSelector").val();
            CCF.pcp.renderSubmitParameters();
        }
      },
	{
        label: 'Cancel',
        isDefault: false,
        close: true
      }
    ]
  })
}


CCF.pcp.renderSubmitParameters = function() {

  CCF.pcp.getSubmissionEntities(CCF.pcp.submitPipeline)

  var url = "/xapi/pipelineControlPanel/project/" + CCF.pcp.project +
    "/pipeline/" + CCF.pcp.submitPipeline + "/submitParametersYaml"

/*
  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities < 1) {
    XNAT.ui.banner.top(3000, "Please select at least one entity before launching")
    return
  }
*/

  XNAT.ui.dialog.open({
    title: CCF.pcp.submitPipeline + " Parameters",
    content:  '<div class="panel">' +
                '<div id="pcp-parameters"></div><hr>' +
                '<div id="pcp-parameters-message"></div>' +
              '</div>',
    beforeShow: function(obj) {
      XNAT.xhr.get({
        url: XNAT.url.rootUrl(url),
        success: function (data) {

          var anyNotOk = false;
          $.each(CCF.pcp.submissionEntities, function(inx, value) {
              if ((value.prereqs === false) || (value.impeded === true)) {
                  anyNotOk = true;
                  return false;
              }
          });
          if (anyNotOk) {
                    xmodal.confirm({
                       title: "Override prereq/impeded indication?",
                       height: 270,
                       width: 580,
                       scroll: false,
                       content: "" +
                         "<p><b>WARNING:  Submission set contains one or more PrereqsMet=FALSE or Impeded=TRUE values.</b></p>" +
                         "<p> One or more of the entities you are submitting are indicated as having prerequisites not met or that the record is impeded (not runnable).  " +
                         "Are you sure you want to continue (Not Recommended)? ",
    		   cancelLabel: 'Continue with Submission (Not Recommended)',
    		   okLabel: 'Cancel Submission',
                       okAction: function(){
                            $('#cancel-launch-button').click();
                            // Do nothing, continue.
                      },
                       cancelAction: function(){
                      },
                    });
          }


          var yml = "";
          if (typeof data !== 'undefined' && $.isArray(data) && data.length>0) {
              for (var i = 0; i<data.length; i++) {
                  if (typeof data[i] === 'string') {
                      yml = yml + data[i];
                  }
              }
          }
          if (yml.length>0) {
            var parsedYaml = YAML.parse(yml);
            XNAT.spawner.spawn(parsedYaml).render('#pcp-parameters');
          } else {
            // clear the loading div
          }
          $('#pcp-parameters-message').append(CCF.pcp.submissionEntities.length +
            " entities selected for launch")
        },
        fail: function () {
          console.log("Failed to get pipeline parameters Yaml")
        }
      });

    },
    buttons: [
    {
        label: 'Launch',
        isDefault: true,
        close: true,
        action: function() {
          // CCF.pcp.getSubmissionParams(this)
          CCF.pcp.submissionParams = {}

          $("#pcp-parameters :input").each(function() {
            if (this.id) {
              CCF.pcp.submissionParams[this.id] = this.value
            }
          })

          var submitUrl = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/pipeline/' + CCF.pcp.submitPipeline + '/pipelineSubmit'
          var submitJson = {
            'entities': CCF.pcp.submissionEntities,
            'parameters': CCF.pcp.submissionParams
          }
          //console.log(JSON.stringify(submitJson))

          $.ajax({
            type: "POST",
            url: submitUrl,
            cache: false,
            async: true,
            contentType: "application/json; charset=utf-8",
            context: this,
            data: JSON.stringify(submitJson),
            dataType: 'json'
          }).done( function(data, textStatus, jqXHR) {
            var msg = CCF.pcp.submitPipeline + ' submitted for ' + CCF.pcp.submissionEntities.length + ' entities'
            XNAT.ui.banner.top(2000, msg);
            CCF.pcp.renderPipelineTable(true)
          }).fail( function(data, textStatus, jqXHR) {
            XNAT.ui.dialog.alert("ERROR: Pipeline launch failed (STATUS=" + textStatus + ").");
          });
        }
      },
			{
        label: 'Cancel',
        id: 'cancel-launch-button',
        isDefault: false,
        close: true
      }
    ]
  })
}

CCF.pcp.getSubmissionEntities = function(pipeline) {
  CCF.pcp.submissionEntities = []

  var $checkedEntities = document.querySelectorAll('input[name=entity-check]:checked')
  var entityLabels = []

  $checkedEntities.forEach(function(entity) {
    entityLabels.push(entity.id + "::" + entity.subgroup)
  })

  // Build entities list to submit to PCP
  for (var i = 0; i < entityLabels.length; i++) {
    var entity = entityLabels[i].split('::')[0]
    var subgroup = entityLabels[i].split('::')[1]

    for (var j = 0; j < CCF.pcp.allStatusEntities.length; j++) {
      // console.log(entity)
      // console.log(CCF.pcp.allStatusEntities[j].entityLabel)
      if (entity === CCF.pcp.allStatusEntities[j].entityLabel && subgroup === CCF.pcp.allStatusEntities[j].subGroup) {
        var currentEntity;
        if (typeof pipeline !== "undefined") {
            currentEntity = $.extend(true, {}, CCF.pcp.allStatusEntities[j]);
            currentEntity.pipeline = pipeline;
        } else {
            currentEntity = CCF.pcp.allStatusEntities[j];
        }
        CCF.pcp.submissionEntities.push(currentEntity)
      }
    }
  }
}

CCF.pcp.getStatusEntity = function(entity, group) {
  var url = "/xapi/pipelineControlPanel/project/" + CCF.pcp.project +
    "/pipeline/" + CCF.pcp.pipeline +
    "/entity/" + entity +
    "/group/" + group + "?cached=true"

  $.ajax({
    type: "GET",
    url:  url,
    cache: false,
    async: false,
    context: this,
    dataType: 'json'
  }).success( function(data, textStatus, jqXHR) {
    CCF.pcp.selectedEntity = data
  }).fail( function(data, textStatus, jqXHR) {
    XNAT.ui.dialog.alert(textStatus + ": Could not get entity info for " + entity);
    CCF.pcp.selectedEntity = {}
  });
}

CCF.pcp.getAllStatusEntities = function() {
  var url = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
    '/pipeline/' + CCF.pcp.pipeline +
    '/status?cached=false&condensed=false'

  $.ajax({
    type: "GET",
    url:  url,
    cache: false,
    async: true,
    context: this,
    dataType: 'json'
  }).success( function(data, textStatus, jqXHR) {
    CCF.pcp.allStatusEntities = data
    console.log('done getting all entities')
    // XNAT.ui.dialog.loading.closeAll()
  }).fail( function(data, textStatus, jqXHR) {
    XNAT.ui.dialog.alert(textStatus + ": Could not get entity info");
    CCF.pcp.allStatusEntities = []
    // XNAT.ui.dialog.loading.closeAll()
  });
}

CCF.pcp.updateGroupMap = function(thisData, i, arrLength) {
	var nGroups = Math.ceil(arrLength/CCF.pcp.ENTITY_GROUP_SIZE);
	var entityId = thisData.entityId;
	var thisGroup = Math.ceil((i+1)/(arrLength/nGroups))
	var prevGroup = ( typeof CCF.pcp.groupMap.get(thisGroup-1) !== "undefined") ? CCF.pcp.groupMap.get(thisGroup-1) : [];
	var currGroup
	if (typeof CCF.pcp.groupMap.get(thisGroup) !== "undefined") {
		currGroup = CCF.pcp.groupMap.get(thisGroup);
	} else {
		currGroup = [];
	}
	if (!(prevGroup.includes(entityId) || currGroup.includes(entityId))) {
		currGroup.push(entityId);
	}
	CCF.pcp.groupMap.set(thisGroup, currGroup);
	return "EntityGroup: " + thisGroup;
}

CCF.pcp.updateStatusEntity = function() {
  var url = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
    '/pipeline/' + CCF.pcp.pipeline + '/updateStatusEntity'

  $.ajax({
    type: "POST",
    url: url,
    cache: false,
    async: true,
    contentType: "application/json; charset=utf-8",
    context: this,
    data: JSON.stringify(CCF.pcp.selectedEntity),
    dataType: 'json'
  }).success( function(data, textStatus, jqXHR) {
    XNAT.ui.banner.top(2000, CCF.pcp.selectedEntity.entityLabel + ' entity updated successfully');
  }).fail( function(data, textStatus, jqXHR) {
    XNAT.ui.dialog.alert(textStatus + ": " + CCF.pcp.selectedEntity.entityLabel + " not updated");
  });
}

