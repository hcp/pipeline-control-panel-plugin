
//# sourceURL=pipeline-control-panel-plugin/pipelineControlPanelConfig.js

if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.pcpconfig === 'undefined') {
	CCF.pcpconfig = {
		prevData: [],
		currentTag: "",
		currentProject: ""
			 };
}

CCF.pcpconfig.postDataUpdates = function(action, async) {
	$.ajax({
		type: "POST",
		url:serverRoot + '/xapi/pipelineControlPanelConfig/' + CCF.pcpconfig.currentProject + '/pipelines',
		cache: false,
		async: !(async === false),
		contentType:"application/json; charset=utf-8",
		context: this,
		data: JSON.stringify(CCF.pcpconfig.currentData),
		dataType: 'json'
	}).done( function(data, textStatus, jqXHR) {
		CCF.pcpconfig.displayPipelinesTable(CCF.pcpconfig.currentTag);
		XNAT.ui.banner.top(2000, 'Pipeline configuration ' + action + '.', 'success');
	}).fail( function(data, textStatus, jqXHR) {
		xmodal.message("ERROR:  The pipeline configuration could not be saved. (STATUS=" + textStatus + ").");
		CCF.pcpconfig.currentData = $.extend(true, [], CCF.pcpconfig.prevData);
		CCF.pcpconfig.displayPipelinesTable(CCF.pcpconfig.currentTag);
	}).always( function(data) {
		if (action === "added") {
			CCF.pcpconfig.refreshPipelineCache()
		}
	});
}

CCF.pcpconfig.refreshPipelineCache = function(rPipeline){
	if (typeof rPipeline === 'undefined' || rPipeline.toString().length<1) {
		rPipeline = CCF.pcpconfig.addedPipeline;
	} 
	setTimeout(function() {
		XNAT.ui.banner.top(5000, 'Caching initial data for ' + rPipeline + ' in background');
	}, 2000);
	$.ajax({
		type: "GET",
		url: serverRoot + '/xapi/pipelineControlPanel/project/'+CCF.pcpconfig.currentProject+'/pipeline/'+rPipeline+'/status?cached=false',
		cache: false,
		async: true,
		contentType:"application/json; charset=utf-8",
		// context: this,
	}).done( function(data, textStatus, jqXHR) {
		// CCF.pcpconfig.displayPipelinesTable(CCF.pcpconfig.currentTag);
		//console.log("successfully loaded pipeline cache")
	}).fail( function(data, textStatus, jqXHR) {
		if (data.status !== 409) {
			xmodal.message("WARNING:  The pipeline control panel cache could not be updated at this time. (STATUS=" + datajqXHR.textStatus + ").");
		} else {
			console.log("WARNING:  A pipeline control panel cache update is already running.  This request was skipped");
		}
		CCF.pcpconfig.currentData = $.extend(true, [], CCF.pcpconfig.prevData);
		CCF.pcpconfig.displayPipelinesTable(CCF.pcpconfig.currentTag);
	});
}

CCF.pcpconfig.veLoaded = false;

CCF.pcpconfig.veChanger = function(e,isKeyPress) {

	var targetName;
	if (typeof e !== 'undefined' && typeof e.currentTarget !== undefined) {
		targetName = $(e.currentTarget).attr('name');
	}

	if (!CCF.pcpconfig.veLoaded) {
		return;
	}
	var pipeline = $("[name=input-pipeline]").val();
	var description = $("[name=input-description]").val();
	var updateFrequency = $("[name=input-updateFrequency]").val();
	var selector = $("[name=input-selector]").val();
	var submitter = $("[name=input-submitter]").val();
	var statusUpdater = $("[name=input-statusUpdater]").val();
	var prereqChecker = $("[name=input-prereqChecker]").val();
	var validator = $("[name=input-validator]").val();
	var execManager = $("[name=input-execManager]").val();
	var configurables = {};
	var allConfigurablesAssigned = true;
	$.each(CCF.pcpconfig.configurableEles, function(index, value) {
		configurables[value] = $("[name=" + value + "]").val();
		// yaml parameters fields don't need to have values
		if (value.toLowerCase().includes("param") && value.toLowerCase().includes("yaml")) {
			return true;
		}
		if (typeof configurables[value] === 'undefined' || configurables[value] == null || configurables[value] == '') {
			allConfigurablesAssigned = false;
			configOk = false;
		}
	});

	if (!(	(!allConfigurablesAssigned) ||  
		typeof pipeline === 'undefined' || pipeline == '' || pipeline == null ||  
		typeof description === 'undefined' || description == '' || description == null || 
		typeof updateFrequency === 'undefined' || updateFrequency == '' || updateFrequency == null ||  
		typeof selector === 'undefined' || selector == '' || selector == null ||  
		typeof submitter === 'undefined' || submitter == '' || submitter == null ||  
		typeof statusUpdater === 'undefined' || statusUpdater == '' || statusUpdater == null || 
		typeof prereqChecker === 'undefined' || prereqChecker == '' || prereqChecker == null || 
		typeof validator === 'undefined' || validator == '' || validator == null ||  
		typeof execManager === 'undefined' || execManager == '' || execManager == null )) {
		$("#view-modal-ok-button").prop('disabled', false);
	} 
	CCF.pcpconfig.pipelineInfo.pipeline = pipeline;
	CCF.pcpconfig.pipelineInfo.description = description;
	CCF.pcpconfig.pipelineInfo.updateFrequency = updateFrequency;
	CCF.pcpconfig.pipelineInfo.selector = selector;
	CCF.pcpconfig.pipelineInfo.submitter = submitter;
	CCF.pcpconfig.pipelineInfo.statusUpdater = statusUpdater;
	CCF.pcpconfig.pipelineInfo.prereqChecker = prereqChecker;
	CCF.pcpconfig.pipelineInfo.validator = validator;
	CCF.pcpconfig.pipelineInfo.execManager = execManager;
	CCF.pcpconfig.pipelineInfo.configurables = configurables;
	//$("#viewPipelinesDiv").html("");
	// TODO:  Do this better
	if (isKeyPress) {
		return;
	}
	$("#viewPipelinesDiv").children().addClass("pleaseRemove");
	XNAT.spawner.spawn(CCF.pcpconfig.viewConfig()).render($("#viewPipelinesDiv")[0]);
	setTimeout(function() { 
		$(".pleaseRemove").remove();
		CCF.pcpconfig.addConfigurablesEvents();
		$(".pcpconfig-ele").keydown(
			function(e) {
				e.stopPropagation();
				CCF.pcpconfig.veChanger(e,true);
			}
		);
		$(".pcpconfig-ele").change(
			function(e) {
				e.stopPropagation();
				CCF.pcpconfig.veChanger(e,false);
			}
		);
		if (typeof targetName !== 'undefined') {
			try {
				$("[name=" + targetName + "]").closest('.panel-element, .panel-default').next('.panel-element, .panel-default').find("input, select").focus();
			} catch(e) {}
		}
	 }, 50)
}

CCF.pcpconfig.addConfigurablesEvents = function() {
	$.each(CCF.pcpconfig.configurableEles, function(index, value) {
		$("[name=" + value + "],#" + value).keydown(
			function(e) {
				e.stopPropagation();
				CCF.pcpconfig.veChanger(e,true);
			}
		);
		$("[name=" + value + "],#" + value).change(
			function(e) {
				e.stopPropagation();
				CCF.pcpconfig.veChanger(e,false);
			}
		);
	});
}

CCF.pcpconfig.viewConfig = function() {

	var viewConfig = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				pipeline: {
					kind: 'panel.input.text',
					name: 'input-pipeline',
					className: 'required pcpconfig-ele',
					size: '30',
					label: 'Pipeline',
					validation: 'alphaDash',
					value: CCF.pcpconfig.pipelineInfo.pipeline
				},
				description: {
					kind: 'panel.input.text',
					name: 'input-description',
					className: 'required pcpconfig-ele',
					size: '30',
					label: 'Description',
					value: CCF.pcpconfig.pipelineInfo.description
				},
				updateFrequency: {
					kind: 'panel.select.single',
					name: 'input-updateFrequency',
					className: 'required pcpconfig-ele',
					label: 'Cache Update Frequency',
					element: { style: { 'padding-left':'10px', 'padding-right':'10px' } },
					value: CCF.pcpconfig.pipelineInfo.updateFrequency,
					options: CCF.pcpconfig.updateFrequencyOptions
				},
				selector: {
					kind: 'panel.select.menu',
					name: 'input-selector',
					className: 'required pcpconfig-ele',
					label: 'Subject/Session Selector',
					element: { style: { 'padding-left':'10px', 'padding-right':'10px' } },
					value: CCF.pcpconfig.pipelineInfo.selector,
					options: CCF.pcpconfig.selectorOptions
				},
				selectorConfig: { },
				submitter: {
					kind: 'panel.select.menu',
					name: 'input-submitter',
					className: 'required pcpconfig-ele',
					label: 'Submitter',
					element: { style: { 'padding-left':'10px', 'padding-right':'10px' } },
					value: CCF.pcpconfig.pipelineInfo.submitter,
					options: CCF.pcpconfig.submitterOptions
				},
				submitterConfig: { },
				statusUpdater: {
					kind: 'panel.select.menu',
					name: 'input-statusUpdater',
					className: 'required pcpconfig-ele',
					label: 'Status Updater',
					element: { style: { 'padding-left':'10px', 'padding-right':'10px' } },
					value: CCF.pcpconfig.pipelineInfo.statusUpdater,
					options: CCF.pcpconfig.statusUpdaterOptions
				},
				statusUpdaterConfig: { },
				prereqChecker: {
					kind: 'panel.select.menu',
					name: 'input-prereqChecker',
					className: 'required pcpconfig-ele',
					label: 'Prereq Checker',
					element: { style: { 'padding-left':'10px', 'padding-right':'10px' } },
					value: CCF.pcpconfig.pipelineInfo.prereqChecker,
					options: CCF.pcpconfig.prereqCheckerOptions
				},
				prereqCheckerConfig: { },
				validator: {
					kind: 'panel.select.menu',
					name: 'input-validator',
					className: 'required pcpconfig-ele',
					label: 'Validator',
					element: { style: { 'padding-left':'10px', 'padding-right':'10px' } },
					value: CCF.pcpconfig.pipelineInfo.validator,
					options: CCF.pcpconfig.validatorOptions
				},
				validatorConfig: { },
				execManager: {
					kind: 'panel.select.menu',
					name: 'input-execManager',
					className: 'required pcpconfig-ele',
					label: 'Execution Manager',
					element: { style: { 'padding-left':'10px', 'padding-right':'10px' } },
					value: CCF.pcpconfig.pipelineInfo.execManager,
					options: CCF.pcpconfig.execManagerOptions
				},
				execManagerConfig: { }
			}
		}
	};
	CCF.pcpconfig.configurableEles = []
	CCF.pcpconfig.populateConfig(viewConfig,"selector");
	CCF.pcpconfig.populateConfig(viewConfig,"submitter");
	CCF.pcpconfig.populateConfig(viewConfig,"statusUpdater");
	CCF.pcpconfig.populateConfig(viewConfig,"prereqChecker");
	CCF.pcpconfig.populateConfig(viewConfig,"validator");
	CCF.pcpconfig.populateConfig(viewConfig,"execManager");
	return viewConfig;

}

CCF.pcpconfig.populateConfig = function(viewConfig,cType) {
	if (typeof CCF.pcpconfig.componentConfig[CCF.pcpconfig.pipelineInfo[cType]] !== 'undefined') {
		viewConfig.viewPanel.contents[ cType + "Config" ] = {
			kind: 'panel',
			header: false,
			footer: false,
			element: { style: { 'padding-top':'0px', 'padding-bottom':'20px' } },
			contents: { }
		};
		var componentConfig = CCF.pcpconfig.componentConfig[CCF.pcpconfig.pipelineInfo[cType]];
		//console.log(componentConfig);
		for (var i=0; i<componentConfig.length; i++) {
			//console.log(componentConfig[i]);
			var parsedComponents = YAML.parse(componentConfig[i]);
			//console.log(parsedComponents);
			for (var prop in parsedComponents) {
				if (parsedComponents.hasOwnProperty(prop)) {
					var propVal = parsedComponents[prop];
					if (typeof propVal.kind !== 'undefined' && typeof propVal.name !== 'undefined') {
						viewConfig.viewPanel.contents[cType + "Config"].contents[prop] = parsedComponents[prop];
						// TODO:  What if there's an existing element?
						try {
							propVal.element = { style: { 'padding-left':'10px', 'padding-right':'10px' } };
							var configurableVal = CCF.pcpconfig.pipelineInfo.configurables[propVal.name];
							propVal.value = ((typeof configurableVal !== 'undefined') ? configurableVal : '');
						} catch (e) {}
						CCF.pcpconfig.configurableEles.push(propVal.name);
					}
				}
			}
			
		}
	} else {
		viewConfig.viewPanel.contents[cType + "Config"] = {};
	}
}

CCF.pcpconfig.pipelineInfoPanel = function(pipeline) {

	var isNew = (typeof pipeline === 'undefined');

	CCF.pcpconfig.veLoaded = false;

	xmodal.open({
		title: ((isNew) ? ' New' : 'View/Edit') + ' PCP Pipeline Configuration',
		id: "view-modal",
		width: 700,
		height: 530,
		content: '<div id="viewPipelinesDiv"></div>',
		okLabel: 'Submit',
		okClose: false,
		cancel: 'Cancel',
		cancelAction: (function() {
			// Need to revert any values changed
			CCF.pcpconfig.populatePipelinesTable('#pipelines-list-container');
			CCF.pcpconfig.viewConfig();
			xmodal.closeAll();
		}),
		okAction: (function() {
			var pipeline = $("[name=input-pipeline]").val();
			var description = $("[name=input-description]").val();
			var updateFrequency = $("[name=input-updateFrequency]").val();
			var selector = $("[name=input-selector]").val();
			var submitter = $("[name=input-submitter]").val();
			var statusUpdater = $("[name=input-statusUpdater]").val();
			var prereqChecker = $("[name=input-prereqChecker]").val();
			var validator = $("[name=input-validator]").val();
			var execManager = $("[name=input-execManager]").val();
			var configurables = {};
			var configOk = true;
			$.each(CCF.pcpconfig.configurableEles, function(index, value) {
				configurables[value] = $("[name=" + value + "]").val();
				// yaml parameters fields don't need to have values
				if (value.toLowerCase().includes("param") && value.toLowerCase().includes("yaml")) {
					return true;
				}
				if (typeof configurables[value] === 'undefined' || configurables[value] == null || configurables[value] == '') {
					configOk = false;
					return false;
				}
			});
			if (typeof pipeline === 'undefined' || pipeline == '' || 
				typeof description === 'undefined' || description == '' || 
				typeof updateFrequency === 'undefined' || updateFrequency == '' || 
				typeof selector === 'undefined' || selector == '' || 
				typeof submitter === 'undefined' || submitter == '' || 
				typeof statusUpdater === 'undefined' || statusUpdater == '' ||
				typeof prereqChecker === 'undefined' || prereqChecker == '' ||
				typeof validator === 'undefined' || validator == '' || 
				typeof execManager === 'undefined' || execManager == '' ||
				!configOk) {
				xmodal.message("ERROR:  Values must be entered for all fields");
			} else if (!pipeline.match(/^[a-z0-9_]+$/gi)) {
				xmodal.message("ERROR:  Pipeline may contain only letters, numbers and underscores");
			} else {
				if (isNew) {
					var alreadyContainsRecord = false;
					$.each(CCF.pcpconfig.currentData, function(index, value) {
						if (value.pipeline === pipeline) {
							xmodal.message("ERROR:  The pipeline indentifier you entered is already in use (pipeline=" + pipeline + ")");
							alreadyContainsData = true;
							return false;
						}
					});
					if (!alreadyContainsRecord) {
						CCF.pcpconfig.currentData.push({
							"pipeline": pipeline,
							"description": description,
							"updateFrequency": updateFrequency,
							"selector": selector,
							"submitter": submitter,
							"statusUpdater": statusUpdater,
							"prereqChecker": prereqChecker,
							"validator": validator,
							"execManager": execManager,
							"configurables": configurables
							});
						xmodal.closeAll();
						CCF.pcpconfig.addedPipeline = pipeline
						CCF.pcpconfig.postDataUpdates('added');
					}
				} else {
					$.each(CCF.pcpconfig.currentData, function(index, value) {
						if (value.pipeline === pipeline) {
							value.description = description;
							value.updateFrequency = updateFrequency;
							value.selector = selector;
							value.submitter = submitter;
							value.statusUpdater = statusUpdater;
							value.prereqChecker = prereqChecker;
							value.validator = validator;
							value.execManager = execManager;
							value.configurables = configurables;
							xmodal.closeAll();
							CCF.pcpconfig.postDataUpdates('updated');
							return false;
						}
					});
				}
			}
		})
	});

	CCF.pcpconfig.pipelineInfo = { pipeline:'', description:'', updateFrequency:'', selector:'', submitter:'', statusUpdater:'', prereqChecker:'', validator:'', execManager:'', configurables:'' };
	if (!isNew) {
		for (var i=0;i<CCF.pcpconfig.currentData.length;i++) {
			if (CCF.pcpconfig.currentData[i].pipeline==pipeline) {
				CCF.pcpconfig.pipelineInfo=CCF.pcpconfig.currentData[i];
			}
		}
	} 
	CCF.pcpconfig.configurableEles = [];
	for (prop in CCF.pcpconfig.pipelineInfo.configurables) {
		CCF.pcpconfig.configurableEles.push(prop);
	}
	XNAT.spawner.spawn(CCF.pcpconfig.viewConfig()).render($("#viewPipelinesDiv")[0]);
	CCF.pcpconfig.addConfigurablesEvents();
	CCF.pcpconfig.veLoaded = true;
	$("#view-modal-ok-button").prop('disabled', true);
	// Not sure why the setTimeout seems necessary here.
	setTimeout(function() {
		$("#view-modal-ok-button").prop('disabled', true);
		if (!isNew) {
			$("#input-pipeline").prop('disabled', true);
		}
		$(".pcpconfig-ele").keydown(
			function(e) {
				e.stopPropagation();
				CCF.pcpconfig.veChanger(e,true);
			}
		);
		$(".pcpconfig-ele").change(
			function(e) {
				e.stopPropagation();
				CCF.pcpconfig.veChanger(e,false);
			}
		);
	 }, 50);

}

CCF.pcpconfig.componentDisplay = function(component) {
	if (typeof component !== 'undefined' && component.includes(".")) {
		return component.substring(component.lastIndexOf(".")+1);
	}
	return component;
}

CCF.pcpconfig.deletePipelineRec = function(pipeline) {

	CCF.pcpconfig.prevData = $.extend(true, [], CCF.pcpconfig.currentData);
	xmodal.confirm({
		title: 'Delete PCP Pipeline Configuration?',
		content: 'Delete PCP Pipeline Configuration: ' + '<b>' + pipeline + '</b>?<br><br>' +
				'NOTE:  This will delete any entity records associated with this pipeline for this project.',
		okLabel: 'Delete',
		okClose: false,
		okAction: function(dlg){
			  var url = "/xapi/pipelineControlPanel/project/" + CCF.pcpconfig.currentProject +
			    "/pipeline/" + pipeline + "/status?cached=true&condensed=true";
			  dlg.close();
			  XNAT.ui.banner.top(2000,"Deleting PCP pipeline " + pipeline + ".");
			  $.ajax({
			    type: "GET",
			    url:  url,
			    cache: false,
			    async: true,
			    context: this,
			    dataType: 'json'
			  }).success( function(data, textStatus, jqXHR) {

				var url = "/xapi/pipelineControlPanel/project/" + CCF.pcpconfig.currentProject +
			    		"/pipeline/" + pipeline + "/deleteStatusEntities";
				$.ajax({
					type: "DELETE",
					url: url,
					cache: false,
					async: true,
					contentType: "application/json; charset=utf-8",
					context: this,
					data: JSON.stringify(data),
					dataType: 'json'
				}).success( function(data, textStatus, jqXHR) {
					for (var i=0; i<CCF.pcpconfig.currentData.length; i++) {
						var pipeInfo = CCF.pcpconfig.currentData[i];
						if (pipeInfo.pipeline == pipeline) {
							CCF.pcpconfig.currentData.splice(i,1);
							CCF.pcpconfig.postDataUpdates('deleted');
							return;
						}
					}
				}).fail( function(data, textStatus, jqXHR) {
				  XNAT.ui.dialog.alert(textStatus + ": Could not remove status entities for pipeline");
				})
			  }).fail( function(data, textStatus, jqXHR) {
			    XNAT.ui.dialog.alert(textStatus + ": Could not remove status entities for pipeline");
			  })
		}
	});
}

CCF.pcpconfig.newPipeline = function() {

	CCF.pcpconfig.prevData = $.extend(true, [], CCF.pcpconfig.currentData);
	CCF.pcpconfig.pipelineInfoPanel(undefined);
	setTimeout(function() { $("[name=input-pipeline]").focus(); }, 100);

}

CCF.pcpconfig.populatePipelinesTable = function(tag) {

	CCF.pcpconfig.currentProject = $(".project-id").html();
	CCF.pcpconfig.currentTag = tag;

	$.ajax({
		type: "GET",
		url:serverRoot+'/xapi/pipelineControlPanelConfig/componentMap',
		cache: false,
		async: true,
		context: this,
		dataType: 'json'
	}).done( function(data, textStatus, jqXHR) {
		var componentMap = data["componentMap"];
		CCF.pcpconfig.componentConfig = data["componentConfig"];
		CCF.pcpconfig.updateFrequencyOptions = {};
		CCF.pcpconfig.selectorOptions = {};
		CCF.pcpconfig.submitterOptions = {};
		CCF.pcpconfig.statusUpdaterOptions = {};
		CCF.pcpconfig.prereqCheckerOptions = {};
		CCF.pcpconfig.validatorOptions = {};
		CCF.pcpconfig.execManagerOptions = {};
		if ('pipelineUpdateFrequency' in componentMap) {
			$.each(componentMap.pipelineUpdateFrequency, function(index, value) {
				var label = value.replace("_"," ");
				CCF.pcpconfig.updateFrequencyOptions["ufOption_" + index]={};
				CCF.pcpconfig.updateFrequencyOptions["ufOption_" + index].label=label;
				CCF.pcpconfig.updateFrequencyOptions["ufOption_" + index].value=value;
			});
		}
		if ('pipelineSelector' in componentMap) {
			componentMap.pipelineSelector.sort();
			$.each(componentMap.pipelineSelector, function(index, value) {
				var label = CCF.pcpconfig.componentDisplay(value);
				CCF.pcpconfig.selectorOptions[label]={};
				CCF.pcpconfig.selectorOptions[label].label=label;
				CCF.pcpconfig.selectorOptions[label].value=value;
			});
		}
		if ('pipelineSubmitter' in componentMap) {
			componentMap.pipelineSubmitter.sort();
			$.each(componentMap.pipelineSubmitter, function(index, value) {
				var label = CCF.pcpconfig.componentDisplay(value);
				CCF.pcpconfig.submitterOptions[label]={};
				CCF.pcpconfig.submitterOptions[label].label=label;
				CCF.pcpconfig.submitterOptions[label].value=value;
			});
		}
		if ('pipelineStatusUpdater' in componentMap) {
			componentMap.pipelineStatusUpdater.sort();
			$.each(componentMap.pipelineStatusUpdater, function(index, value) {
				var label = CCF.pcpconfig.componentDisplay(value);
				CCF.pcpconfig.statusUpdaterOptions[label]={};
				CCF.pcpconfig.statusUpdaterOptions[label].label=label;
				CCF.pcpconfig.statusUpdaterOptions[label].value=value;
			});
		}
		if ('pipelinePrereqChecker' in componentMap) {
			componentMap.pipelinePrereqChecker.sort();
			$.each(componentMap.pipelinePrereqChecker, function(index, value) {
				var label = CCF.pcpconfig.componentDisplay(value);
				CCF.pcpconfig.prereqCheckerOptions[label]={};
				CCF.pcpconfig.prereqCheckerOptions[label].label=label;
				CCF.pcpconfig.prereqCheckerOptions[label].value=value;
			});
		}
		if ('pipelineValidator' in componentMap) {
			componentMap.pipelineValidator.sort();
			$.each(componentMap.pipelineValidator, function(index, value) {
				var label = CCF.pcpconfig.componentDisplay(value);
				CCF.pcpconfig.validatorOptions[label]={};
				CCF.pcpconfig.validatorOptions[label].label=label;
				CCF.pcpconfig.validatorOptions[label].value=value;
			});
		}
		if ('pipelineExecManager' in componentMap) {
			componentMap.pipelineExecManager.sort();
			$.each(componentMap.pipelineExecManager, function(index, value) {
				var label = CCF.pcpconfig.componentDisplay(value);
				CCF.pcpconfig.execManagerOptions[label]={};
				CCF.pcpconfig.execManagerOptions[label].label=label;
				CCF.pcpconfig.execManagerOptions[label].value=value;
			});
		}
		CCF.pcpconfig.continuePopulatingPipelinesTable(tag);
	}).fail( function(data, textStatus, jqXHR) {
		$(CCF.pcpconfig.currentTag).html("ERROR:  Could not retrieve pipeline configuration.");
		$("#create-new-pipeline").hide();
	});
}

CCF.pcpconfig.continuePopulatingPipelinesTable = function(tag) {
	$.ajax({
		type: "GET",
		url:serverRoot+'/xapi/pipelineControlPanelConfig/' + CCF.pcpconfig.currentProject + '/pipelines',
		cache: false,
		async: true,
		context: this,
		dataType: 'json'
	}).done( function(data, textStatus, jqXHR) {
		CCF.pcpconfig.currentData = data;
		if (!$.isArray(CCF.pcpconfig.currentData)) {
			$(CCF.pcpconfig.currentTag).html("ERROR:  The pipeline configuration data returned was invalid (PROBLEM=NOT_AN_ARRAY).");
			$("#create-new-pipeline").hide();
			return;
		}
		CCF.pcpconfig.displayPipelinesTable(tag);
	}).fail( function(data, textStatus, jqXHR) {
		$(CCF.pcpconfig.currentTag).html("ERROR:  Could not retrieve pipeline configuration.");
		$("#create-new-pipeline").hide();
	});
}

CCF.pcpconfig.displayPipelinesTable = function(tag) {

	if (CCF.pcpconfig.currentData.length==0) {
		$(CCF.pcpconfig.currentTag).html("No Pipeline Control Panel pipelines have been configured for this project.");
		return;
	}
	var tableHtml = "<table class='data-table xnat-table' style='width:100%;'>";
	tableHtml+="<tr><th></th><th class='center'>Pipeline</th><th class='center'>Description</th><th></th></tr>";
	CCF.pcpconfig.currentData.sort(function(a,b) {
		return a.pipeline.localeCompare(b.pipeline);
	})
	for (var i=0; i<CCF.pcpconfig.currentData.length; i++) {
		var pipeInfo = CCF.pcpconfig.currentData[i];
		tableHtml+="<tr>";
		tableHtml+="<td class='center'><a href='javascript:CCF.pcpconfig.pipelineInfoPanel(\"" + pipeInfo.pipeline + "\");'>View/Edit</a></td>";
		tableHtml+="<td>" + pipeInfo.pipeline + "</td>";
		tableHtml+="<td>" + pipeInfo.description + "</td>";
		tableHtml+="<td class='center'><button onclick='CCF.pcpconfig.deletePipelineRec(\"" + pipeInfo.pipeline + "\")'>Delete</button></td>";
		tableHtml+="</tr>";
	}
	tableHtml+="</table>";
	$(CCF.pcpconfig.currentTag).html(tableHtml);

}

CCF.pcpconfig.gotoPcpDashboard = function() {
	location.href="/app/template/Pcp.vm?project=" + CCF.pcpconfig.currentProject
}

